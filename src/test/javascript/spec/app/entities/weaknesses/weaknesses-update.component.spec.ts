/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { Observable, of } from 'rxjs';

import { PokemonBattleTestModule } from '../../../test.module';
import { WeaknessesUpdateComponent } from 'app/entities/weaknesses/weaknesses-update.component';
import { WeaknessesService } from 'app/entities/weaknesses/weaknesses.service';
import { Weaknesses } from 'app/shared/model/weaknesses.model';

describe('Component Tests', () => {
    describe('Weaknesses Management Update Component', () => {
        let comp: WeaknessesUpdateComponent;
        let fixture: ComponentFixture<WeaknessesUpdateComponent>;
        let service: WeaknessesService;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [PokemonBattleTestModule],
                declarations: [WeaknessesUpdateComponent]
            })
                .overrideTemplate(WeaknessesUpdateComponent, '')
                .compileComponents();

            fixture = TestBed.createComponent(WeaknessesUpdateComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(WeaknessesService);
        });

        describe('save', () => {
            it(
                'Should call update service on save for existing entity',
                fakeAsync(() => {
                    // GIVEN
                    const entity = new Weaknesses(123);
                    spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
                    comp.weaknesses = entity;
                    // WHEN
                    comp.save();
                    tick(); // simulate async

                    // THEN
                    expect(service.update).toHaveBeenCalledWith(entity);
                    expect(comp.isSaving).toEqual(false);
                })
            );

            it(
                'Should call create service on save for new entity',
                fakeAsync(() => {
                    // GIVEN
                    const entity = new Weaknesses();
                    spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
                    comp.weaknesses = entity;
                    // WHEN
                    comp.save();
                    tick(); // simulate async

                    // THEN
                    expect(service.create).toHaveBeenCalledWith(entity);
                    expect(comp.isSaving).toEqual(false);
                })
            );
        });
    });
});
