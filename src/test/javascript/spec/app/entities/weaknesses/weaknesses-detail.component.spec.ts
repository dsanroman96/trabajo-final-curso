/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { PokemonBattleTestModule } from '../../../test.module';
import { WeaknessesDetailComponent } from 'app/entities/weaknesses/weaknesses-detail.component';
import { Weaknesses } from 'app/shared/model/weaknesses.model';

describe('Component Tests', () => {
    describe('Weaknesses Management Detail Component', () => {
        let comp: WeaknessesDetailComponent;
        let fixture: ComponentFixture<WeaknessesDetailComponent>;
        const route = ({ data: of({ weaknesses: new Weaknesses(123) }) } as any) as ActivatedRoute;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [PokemonBattleTestModule],
                declarations: [WeaknessesDetailComponent],
                providers: [{ provide: ActivatedRoute, useValue: route }]
            })
                .overrideTemplate(WeaknessesDetailComponent, '')
                .compileComponents();
            fixture = TestBed.createComponent(WeaknessesDetailComponent);
            comp = fixture.componentInstance;
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(comp.weaknesses).toEqual(jasmine.objectContaining({ id: 123 }));
            });
        });
    });
});
