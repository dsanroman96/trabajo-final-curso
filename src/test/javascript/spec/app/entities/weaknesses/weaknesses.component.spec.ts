/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { Observable, of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { PokemonBattleTestModule } from '../../../test.module';
import { WeaknessesComponent } from 'app/entities/weaknesses/weaknesses.component';
import { WeaknessesService } from 'app/entities/weaknesses/weaknesses.service';
import { Weaknesses } from 'app/shared/model/weaknesses.model';

describe('Component Tests', () => {
    describe('Weaknesses Management Component', () => {
        let comp: WeaknessesComponent;
        let fixture: ComponentFixture<WeaknessesComponent>;
        let service: WeaknessesService;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [PokemonBattleTestModule],
                declarations: [WeaknessesComponent],
                providers: []
            })
                .overrideTemplate(WeaknessesComponent, '')
                .compileComponents();

            fixture = TestBed.createComponent(WeaknessesComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(WeaknessesService);
        });

        it('Should call load all on init', () => {
            // GIVEN
            const headers = new HttpHeaders().append('link', 'link;link');
            spyOn(service, 'query').and.returnValue(
                of(
                    new HttpResponse({
                        body: [new Weaknesses(123)],
                        headers
                    })
                )
            );

            // WHEN
            comp.ngOnInit();

            // THEN
            expect(service.query).toHaveBeenCalled();
            expect(comp.weaknesses[0]).toEqual(jasmine.objectContaining({ id: 123 }));
        });
    });
});
