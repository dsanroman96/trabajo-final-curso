/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { Observable, of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { PokemonBattleTestModule } from '../../../test.module';
import { ResistancesComponent } from 'app/entities/resistances/resistances.component';
import { ResistancesService } from 'app/entities/resistances/resistances.service';
import { Resistances } from 'app/shared/model/resistances.model';

describe('Component Tests', () => {
    describe('Resistances Management Component', () => {
        let comp: ResistancesComponent;
        let fixture: ComponentFixture<ResistancesComponent>;
        let service: ResistancesService;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [PokemonBattleTestModule],
                declarations: [ResistancesComponent],
                providers: []
            })
                .overrideTemplate(ResistancesComponent, '')
                .compileComponents();

            fixture = TestBed.createComponent(ResistancesComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(ResistancesService);
        });

        it('Should call load all on init', () => {
            // GIVEN
            const headers = new HttpHeaders().append('link', 'link;link');
            spyOn(service, 'query').and.returnValue(
                of(
                    new HttpResponse({
                        body: [new Resistances(123)],
                        headers
                    })
                )
            );

            // WHEN
            comp.ngOnInit();

            // THEN
            expect(service.query).toHaveBeenCalled();
            expect(comp.resistances[0]).toEqual(jasmine.objectContaining({ id: 123 }));
        });
    });
});
