/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { Observable, of } from 'rxjs';

import { PokemonBattleTestModule } from '../../../test.module';
import { ResistancesUpdateComponent } from 'app/entities/resistances/resistances-update.component';
import { ResistancesService } from 'app/entities/resistances/resistances.service';
import { Resistances } from 'app/shared/model/resistances.model';

describe('Component Tests', () => {
    describe('Resistances Management Update Component', () => {
        let comp: ResistancesUpdateComponent;
        let fixture: ComponentFixture<ResistancesUpdateComponent>;
        let service: ResistancesService;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [PokemonBattleTestModule],
                declarations: [ResistancesUpdateComponent]
            })
                .overrideTemplate(ResistancesUpdateComponent, '')
                .compileComponents();

            fixture = TestBed.createComponent(ResistancesUpdateComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(ResistancesService);
        });

        describe('save', () => {
            it(
                'Should call update service on save for existing entity',
                fakeAsync(() => {
                    // GIVEN
                    const entity = new Resistances(123);
                    spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
                    comp.resistances = entity;
                    // WHEN
                    comp.save();
                    tick(); // simulate async

                    // THEN
                    expect(service.update).toHaveBeenCalledWith(entity);
                    expect(comp.isSaving).toEqual(false);
                })
            );

            it(
                'Should call create service on save for new entity',
                fakeAsync(() => {
                    // GIVEN
                    const entity = new Resistances();
                    spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
                    comp.resistances = entity;
                    // WHEN
                    comp.save();
                    tick(); // simulate async

                    // THEN
                    expect(service.create).toHaveBeenCalledWith(entity);
                    expect(comp.isSaving).toEqual(false);
                })
            );
        });
    });
});
