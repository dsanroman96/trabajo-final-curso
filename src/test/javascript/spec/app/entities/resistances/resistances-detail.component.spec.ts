/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { PokemonBattleTestModule } from '../../../test.module';
import { ResistancesDetailComponent } from 'app/entities/resistances/resistances-detail.component';
import { Resistances } from 'app/shared/model/resistances.model';

describe('Component Tests', () => {
    describe('Resistances Management Detail Component', () => {
        let comp: ResistancesDetailComponent;
        let fixture: ComponentFixture<ResistancesDetailComponent>;
        const route = ({ data: of({ resistances: new Resistances(123) }) } as any) as ActivatedRoute;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [PokemonBattleTestModule],
                declarations: [ResistancesDetailComponent],
                providers: [{ provide: ActivatedRoute, useValue: route }]
            })
                .overrideTemplate(ResistancesDetailComponent, '')
                .compileComponents();
            fixture = TestBed.createComponent(ResistancesDetailComponent);
            comp = fixture.componentInstance;
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(comp.resistances).toEqual(jasmine.objectContaining({ id: 123 }));
            });
        });
    });
});
