/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { PokemonBattleTestModule } from '../../../test.module';
import { MoveDetailComponent } from 'app/entities/move/move-detail.component';
import { Move } from 'app/shared/model/move.model';

describe('Component Tests', () => {
    describe('Move Management Detail Component', () => {
        let comp: MoveDetailComponent;
        let fixture: ComponentFixture<MoveDetailComponent>;
        const route = ({ data: of({ move: new Move(123) }) } as any) as ActivatedRoute;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [PokemonBattleTestModule],
                declarations: [MoveDetailComponent],
                providers: [{ provide: ActivatedRoute, useValue: route }]
            })
                .overrideTemplate(MoveDetailComponent, '')
                .compileComponents();
            fixture = TestBed.createComponent(MoveDetailComponent);
            comp = fixture.componentInstance;
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(comp.move).toEqual(jasmine.objectContaining({ id: 123 }));
            });
        });
    });
});
