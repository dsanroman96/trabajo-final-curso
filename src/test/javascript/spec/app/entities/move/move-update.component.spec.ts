/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { Observable, of } from 'rxjs';

import { PokemonBattleTestModule } from '../../../test.module';
import { MoveUpdateComponent } from 'app/entities/move/move-update.component';
import { MoveService } from 'app/entities/move/move.service';
import { Move } from 'app/shared/model/move.model';

describe('Component Tests', () => {
    describe('Move Management Update Component', () => {
        let comp: MoveUpdateComponent;
        let fixture: ComponentFixture<MoveUpdateComponent>;
        let service: MoveService;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [PokemonBattleTestModule],
                declarations: [MoveUpdateComponent]
            })
                .overrideTemplate(MoveUpdateComponent, '')
                .compileComponents();

            fixture = TestBed.createComponent(MoveUpdateComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(MoveService);
        });

        describe('save', () => {
            it(
                'Should call update service on save for existing entity',
                fakeAsync(() => {
                    // GIVEN
                    const entity = new Move(123);
                    spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
                    comp.move = entity;
                    // WHEN
                    comp.save();
                    tick(); // simulate async

                    // THEN
                    expect(service.update).toHaveBeenCalledWith(entity);
                    expect(comp.isSaving).toEqual(false);
                })
            );

            it(
                'Should call create service on save for new entity',
                fakeAsync(() => {
                    // GIVEN
                    const entity = new Move();
                    spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
                    comp.move = entity;
                    // WHEN
                    comp.save();
                    tick(); // simulate async

                    // THEN
                    expect(service.create).toHaveBeenCalledWith(entity);
                    expect(comp.isSaving).toEqual(false);
                })
            );
        });
    });
});
