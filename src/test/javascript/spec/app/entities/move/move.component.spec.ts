/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { Observable, of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { PokemonBattleTestModule } from '../../../test.module';
import { MoveComponent } from 'app/entities/move/move.component';
import { MoveService } from 'app/entities/move/move.service';
import { Move } from 'app/shared/model/move.model';

describe('Component Tests', () => {
    describe('Move Management Component', () => {
        let comp: MoveComponent;
        let fixture: ComponentFixture<MoveComponent>;
        let service: MoveService;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [PokemonBattleTestModule],
                declarations: [MoveComponent],
                providers: []
            })
                .overrideTemplate(MoveComponent, '')
                .compileComponents();

            fixture = TestBed.createComponent(MoveComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(MoveService);
        });

        it('Should call load all on init', () => {
            // GIVEN
            const headers = new HttpHeaders().append('link', 'link;link');
            spyOn(service, 'query').and.returnValue(
                of(
                    new HttpResponse({
                        body: [new Move(123)],
                        headers
                    })
                )
            );

            // WHEN
            comp.ngOnInit();

            // THEN
            expect(service.query).toHaveBeenCalled();
            expect(comp.moves[0]).toEqual(jasmine.objectContaining({ id: 123 }));
        });
    });
});
