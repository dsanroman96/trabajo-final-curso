/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable, of } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

import { PokemonBattleTestModule } from '../../../test.module';
import { MoveDeleteDialogComponent } from 'app/entities/move/move-delete-dialog.component';
import { MoveService } from 'app/entities/move/move.service';

describe('Component Tests', () => {
    describe('Move Management Delete Component', () => {
        let comp: MoveDeleteDialogComponent;
        let fixture: ComponentFixture<MoveDeleteDialogComponent>;
        let service: MoveService;
        let mockEventManager: any;
        let mockActiveModal: any;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [PokemonBattleTestModule],
                declarations: [MoveDeleteDialogComponent]
            })
                .overrideTemplate(MoveDeleteDialogComponent, '')
                .compileComponents();
            fixture = TestBed.createComponent(MoveDeleteDialogComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(MoveService);
            mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
            mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
        });

        describe('confirmDelete', () => {
            it('Should call delete service on confirmDelete', inject(
                [],
                fakeAsync(() => {
                    // GIVEN
                    spyOn(service, 'delete').and.returnValue(of({}));

                    // WHEN
                    comp.confirmDelete(123);
                    tick();

                    // THEN
                    expect(service.delete).toHaveBeenCalledWith(123);
                    expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                    expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
                })
            ));
        });
    });
});
