/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { Observable, of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { PokemonBattleTestModule } from '../../../test.module';
import { PokemonComponent } from 'app/entities/pokemon/pokemon.component';
import { PokemonService } from 'app/entities/pokemon/pokemon.service';
import { Pokemon } from 'app/shared/model/pokemon.model';

describe('Component Tests', () => {
    describe('Pokemon Management Component', () => {
        let comp: PokemonComponent;
        let fixture: ComponentFixture<PokemonComponent>;
        let service: PokemonService;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [PokemonBattleTestModule],
                declarations: [PokemonComponent],
                providers: []
            })
                .overrideTemplate(PokemonComponent, '')
                .compileComponents();

            fixture = TestBed.createComponent(PokemonComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(PokemonService);
        });

        it('Should call load all on init', () => {
            // GIVEN
            const headers = new HttpHeaders().append('link', 'link;link');
            spyOn(service, 'query').and.returnValue(
                of(
                    new HttpResponse({
                        body: [new Pokemon(123)],
                        headers
                    })
                )
            );

            // WHEN
            comp.ngOnInit();

            // THEN
            expect(service.query).toHaveBeenCalled();
            expect(comp.pokemons[0]).toEqual(jasmine.objectContaining({ id: 123 }));
        });
    });
});
