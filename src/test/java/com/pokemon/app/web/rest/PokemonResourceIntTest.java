package com.pokemon.app.web.rest;

import com.pokemon.app.PokemonBattleApp;

import com.pokemon.app.domain.Pokemon;
import com.pokemon.app.repository.PokemonRepository;
import com.pokemon.app.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.util.ArrayList;
import java.util.List;


import static com.pokemon.app.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the PokemonResource REST controller.
 *
 * @see PokemonResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = PokemonBattleApp.class)
public class PokemonResourceIntTest {

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    private static final Integer DEFAULT_ATTACK = 1;
    private static final Integer UPDATED_ATTACK = 2;

    private static final Integer DEFAULT_SP_ATTACK = 1;
    private static final Integer UPDATED_SP_ATTACK = 2;

    private static final Integer DEFAULT_DEFENSE = 1;
    private static final Integer UPDATED_DEFENSE = 2;

    private static final Integer DEFAULT_SP_DEFENSE = 1;
    private static final Integer UPDATED_SP_DEFENSE = 2;

    private static final Integer DEFAULT_SPEED = 1;
    private static final Integer UPDATED_SPEED = 2;

    private static final Integer DEFAULT_HP = 1;
    private static final Integer UPDATED_HP = 2;

    private static final Boolean DEFAULT_DEFEATED = false;
    private static final Boolean UPDATED_DEFEATED = true;

    @Autowired
    private PokemonRepository pokemonRepository;

    @Mock
    private PokemonRepository pokemonRepositoryMock;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restPokemonMockMvc;

    private Pokemon pokemon;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final PokemonResource pokemonResource = new PokemonResource(pokemonRepository);
        this.restPokemonMockMvc = MockMvcBuilders.standaloneSetup(pokemonResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Pokemon createEntity(EntityManager em) {
        Pokemon pokemon = new Pokemon()
            .name(DEFAULT_NAME)
            .attack(DEFAULT_ATTACK)
            .spAttack(DEFAULT_SP_ATTACK)
            .defense(DEFAULT_DEFENSE)
            .spDefense(DEFAULT_SP_DEFENSE)
            .speed(DEFAULT_SPEED)
            .hp(DEFAULT_HP)
            .defeated(DEFAULT_DEFEATED);
        return pokemon;
    }

    @Before
    public void initTest() {
        pokemon = createEntity(em);
    }

    @Test
    @Transactional
    public void createPokemon() throws Exception {
        int databaseSizeBeforeCreate = pokemonRepository.findAll().size();

        // Create the Pokemon
        restPokemonMockMvc.perform(post("/api/pokemons")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(pokemon)))
            .andExpect(status().isCreated());

        // Validate the Pokemon in the database
        List<Pokemon> pokemonList = pokemonRepository.findAll();
        assertThat(pokemonList).hasSize(databaseSizeBeforeCreate + 1);
        Pokemon testPokemon = pokemonList.get(pokemonList.size() - 1);
        assertThat(testPokemon.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testPokemon.getAttack()).isEqualTo(DEFAULT_ATTACK);
        assertThat(testPokemon.getSpAttack()).isEqualTo(DEFAULT_SP_ATTACK);
        assertThat(testPokemon.getDefense()).isEqualTo(DEFAULT_DEFENSE);
        assertThat(testPokemon.getSpDefense()).isEqualTo(DEFAULT_SP_DEFENSE);
        assertThat(testPokemon.getSpeed()).isEqualTo(DEFAULT_SPEED);
        assertThat(testPokemon.getHp()).isEqualTo(DEFAULT_HP);
        assertThat(testPokemon.isDefeated()).isEqualTo(DEFAULT_DEFEATED);
    }

    @Test
    @Transactional
    public void createPokemonWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = pokemonRepository.findAll().size();

        // Create the Pokemon with an existing ID
        pokemon.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restPokemonMockMvc.perform(post("/api/pokemons")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(pokemon)))
            .andExpect(status().isBadRequest());

        // Validate the Pokemon in the database
        List<Pokemon> pokemonList = pokemonRepository.findAll();
        assertThat(pokemonList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllPokemons() throws Exception {
        // Initialize the database
        pokemonRepository.saveAndFlush(pokemon);

        // Get all the pokemonList
        restPokemonMockMvc.perform(get("/api/pokemons?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(pokemon.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())))
            .andExpect(jsonPath("$.[*].attack").value(hasItem(DEFAULT_ATTACK)))
            .andExpect(jsonPath("$.[*].spAttack").value(hasItem(DEFAULT_SP_ATTACK)))
            .andExpect(jsonPath("$.[*].defense").value(hasItem(DEFAULT_DEFENSE)))
            .andExpect(jsonPath("$.[*].spDefense").value(hasItem(DEFAULT_SP_DEFENSE)))
            .andExpect(jsonPath("$.[*].speed").value(hasItem(DEFAULT_SPEED)))
            .andExpect(jsonPath("$.[*].hp").value(hasItem(DEFAULT_HP)))
            .andExpect(jsonPath("$.[*].defeated").value(hasItem(DEFAULT_DEFEATED.booleanValue())));
    }
    
    @SuppressWarnings({"unchecked"})
    public void getAllPokemonsWithEagerRelationshipsIsEnabled() throws Exception {
        PokemonResource pokemonResource = new PokemonResource(pokemonRepositoryMock);
        when(pokemonRepositoryMock.findAllWithEagerRelationships(any())).thenReturn(new PageImpl(new ArrayList<>()));

        MockMvc restPokemonMockMvc = MockMvcBuilders.standaloneSetup(pokemonResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();

        restPokemonMockMvc.perform(get("/api/pokemons?eagerload=true"))
        .andExpect(status().isOk());

        verify(pokemonRepositoryMock, times(1)).findAllWithEagerRelationships(any());
    }

    @SuppressWarnings({"unchecked"})
    public void getAllPokemonsWithEagerRelationshipsIsNotEnabled() throws Exception {
        PokemonResource pokemonResource = new PokemonResource(pokemonRepositoryMock);
            when(pokemonRepositoryMock.findAllWithEagerRelationships(any())).thenReturn(new PageImpl(new ArrayList<>()));
            MockMvc restPokemonMockMvc = MockMvcBuilders.standaloneSetup(pokemonResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();

        restPokemonMockMvc.perform(get("/api/pokemons?eagerload=true"))
        .andExpect(status().isOk());

            verify(pokemonRepositoryMock, times(1)).findAllWithEagerRelationships(any());
    }

    @Test
    @Transactional
    public void getPokemon() throws Exception {
        // Initialize the database
        pokemonRepository.saveAndFlush(pokemon);

        // Get the pokemon
        restPokemonMockMvc.perform(get("/api/pokemons/{id}", pokemon.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(pokemon.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME.toString()))
            .andExpect(jsonPath("$.attack").value(DEFAULT_ATTACK))
            .andExpect(jsonPath("$.spAttack").value(DEFAULT_SP_ATTACK))
            .andExpect(jsonPath("$.defense").value(DEFAULT_DEFENSE))
            .andExpect(jsonPath("$.spDefense").value(DEFAULT_SP_DEFENSE))
            .andExpect(jsonPath("$.speed").value(DEFAULT_SPEED))
            .andExpect(jsonPath("$.hp").value(DEFAULT_HP))
            .andExpect(jsonPath("$.defeated").value(DEFAULT_DEFEATED.booleanValue()));
    }

    @Test
    @Transactional
    public void getNonExistingPokemon() throws Exception {
        // Get the pokemon
        restPokemonMockMvc.perform(get("/api/pokemons/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updatePokemon() throws Exception {
        // Initialize the database
        pokemonRepository.saveAndFlush(pokemon);

        int databaseSizeBeforeUpdate = pokemonRepository.findAll().size();

        // Update the pokemon
        Pokemon updatedPokemon = pokemonRepository.findById(pokemon.getId()).get();
        // Disconnect from session so that the updates on updatedPokemon are not directly saved in db
        em.detach(updatedPokemon);
        updatedPokemon
            .name(UPDATED_NAME)
            .attack(UPDATED_ATTACK)
            .spAttack(UPDATED_SP_ATTACK)
            .defense(UPDATED_DEFENSE)
            .spDefense(UPDATED_SP_DEFENSE)
            .speed(UPDATED_SPEED)
            .hp(UPDATED_HP)
            .defeated(UPDATED_DEFEATED);

        restPokemonMockMvc.perform(put("/api/pokemons")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedPokemon)))
            .andExpect(status().isOk());

        // Validate the Pokemon in the database
        List<Pokemon> pokemonList = pokemonRepository.findAll();
        assertThat(pokemonList).hasSize(databaseSizeBeforeUpdate);
        Pokemon testPokemon = pokemonList.get(pokemonList.size() - 1);
        assertThat(testPokemon.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testPokemon.getAttack()).isEqualTo(UPDATED_ATTACK);
        assertThat(testPokemon.getSpAttack()).isEqualTo(UPDATED_SP_ATTACK);
        assertThat(testPokemon.getDefense()).isEqualTo(UPDATED_DEFENSE);
        assertThat(testPokemon.getSpDefense()).isEqualTo(UPDATED_SP_DEFENSE);
        assertThat(testPokemon.getSpeed()).isEqualTo(UPDATED_SPEED);
        assertThat(testPokemon.getHp()).isEqualTo(UPDATED_HP);
        assertThat(testPokemon.isDefeated()).isEqualTo(UPDATED_DEFEATED);
    }

    @Test
    @Transactional
    public void updateNonExistingPokemon() throws Exception {
        int databaseSizeBeforeUpdate = pokemonRepository.findAll().size();

        // Create the Pokemon

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restPokemonMockMvc.perform(put("/api/pokemons")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(pokemon)))
            .andExpect(status().isBadRequest());

        // Validate the Pokemon in the database
        List<Pokemon> pokemonList = pokemonRepository.findAll();
        assertThat(pokemonList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deletePokemon() throws Exception {
        // Initialize the database
        pokemonRepository.saveAndFlush(pokemon);

        int databaseSizeBeforeDelete = pokemonRepository.findAll().size();

        // Delete the pokemon
        restPokemonMockMvc.perform(delete("/api/pokemons/{id}", pokemon.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<Pokemon> pokemonList = pokemonRepository.findAll();
        assertThat(pokemonList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Pokemon.class);
        Pokemon pokemon1 = new Pokemon();
        pokemon1.setId(1L);
        Pokemon pokemon2 = new Pokemon();
        pokemon2.setId(pokemon1.getId());
        assertThat(pokemon1).isEqualTo(pokemon2);
        pokemon2.setId(2L);
        assertThat(pokemon1).isNotEqualTo(pokemon2);
        pokemon1.setId(null);
        assertThat(pokemon1).isNotEqualTo(pokemon2);
    }
}
