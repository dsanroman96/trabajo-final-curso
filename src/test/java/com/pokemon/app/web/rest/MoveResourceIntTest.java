package com.pokemon.app.web.rest;

import com.pokemon.app.PokemonBattleApp;

import com.pokemon.app.domain.Move;
import com.pokemon.app.repository.MoveRepository;
import com.pokemon.app.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.util.List;


import static com.pokemon.app.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the MoveResource REST controller.
 *
 * @see MoveResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = PokemonBattleApp.class)
public class MoveResourceIntTest {

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    private static final Integer DEFAULT_POWER = 1;
    private static final Integer UPDATED_POWER = 2;

    private static final Integer DEFAULT_ACCURACY = 1;
    private static final Integer UPDATED_ACCURACY = 2;

    private static final Integer DEFAULT_PP = 1;
    private static final Integer UPDATED_PP = 2;

    private static final Boolean DEFAULT_CONTACT = false;
    private static final Boolean UPDATED_CONTACT = true;

    @Autowired
    private MoveRepository moveRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restMoveMockMvc;

    private Move move;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final MoveResource moveResource = new MoveResource(moveRepository);
        this.restMoveMockMvc = MockMvcBuilders.standaloneSetup(moveResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Move createEntity(EntityManager em) {
        Move move = new Move()
            .name(DEFAULT_NAME)
            .power(DEFAULT_POWER)
            .accuracy(DEFAULT_ACCURACY)
            .pp(DEFAULT_PP)
            .contact(DEFAULT_CONTACT);
        return move;
    }

    @Before
    public void initTest() {
        move = createEntity(em);
    }

    @Test
    @Transactional
    public void createMove() throws Exception {
        int databaseSizeBeforeCreate = moveRepository.findAll().size();

        // Create the Move
        restMoveMockMvc.perform(post("/api/moves")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(move)))
            .andExpect(status().isCreated());

        // Validate the Move in the database
        List<Move> moveList = moveRepository.findAll();
        assertThat(moveList).hasSize(databaseSizeBeforeCreate + 1);
        Move testMove = moveList.get(moveList.size() - 1);
        assertThat(testMove.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testMove.getPower()).isEqualTo(DEFAULT_POWER);
        assertThat(testMove.getAccuracy()).isEqualTo(DEFAULT_ACCURACY);
        assertThat(testMove.getPp()).isEqualTo(DEFAULT_PP);
        assertThat(testMove.isContact()).isEqualTo(DEFAULT_CONTACT);
    }

    @Test
    @Transactional
    public void createMoveWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = moveRepository.findAll().size();

        // Create the Move with an existing ID
        move.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restMoveMockMvc.perform(post("/api/moves")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(move)))
            .andExpect(status().isBadRequest());

        // Validate the Move in the database
        List<Move> moveList = moveRepository.findAll();
        assertThat(moveList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllMoves() throws Exception {
        // Initialize the database
        moveRepository.saveAndFlush(move);

        // Get all the moveList
        restMoveMockMvc.perform(get("/api/moves?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(move.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())))
            .andExpect(jsonPath("$.[*].power").value(hasItem(DEFAULT_POWER)))
            .andExpect(jsonPath("$.[*].accuracy").value(hasItem(DEFAULT_ACCURACY)))
            .andExpect(jsonPath("$.[*].pp").value(hasItem(DEFAULT_PP)))
            .andExpect(jsonPath("$.[*].contact").value(hasItem(DEFAULT_CONTACT.booleanValue())));
    }
    
    @Test
    @Transactional
    public void getMove() throws Exception {
        // Initialize the database
        moveRepository.saveAndFlush(move);

        // Get the move
        restMoveMockMvc.perform(get("/api/moves/{id}", move.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(move.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME.toString()))
            .andExpect(jsonPath("$.power").value(DEFAULT_POWER))
            .andExpect(jsonPath("$.accuracy").value(DEFAULT_ACCURACY))
            .andExpect(jsonPath("$.pp").value(DEFAULT_PP))
            .andExpect(jsonPath("$.contact").value(DEFAULT_CONTACT.booleanValue()));
    }

    @Test
    @Transactional
    public void getNonExistingMove() throws Exception {
        // Get the move
        restMoveMockMvc.perform(get("/api/moves/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateMove() throws Exception {
        // Initialize the database
        moveRepository.saveAndFlush(move);

        int databaseSizeBeforeUpdate = moveRepository.findAll().size();

        // Update the move
        Move updatedMove = moveRepository.findById(move.getId()).get();
        // Disconnect from session so that the updates on updatedMove are not directly saved in db
        em.detach(updatedMove);
        updatedMove
            .name(UPDATED_NAME)
            .power(UPDATED_POWER)
            .accuracy(UPDATED_ACCURACY)
            .pp(UPDATED_PP)
            .contact(UPDATED_CONTACT);

        restMoveMockMvc.perform(put("/api/moves")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedMove)))
            .andExpect(status().isOk());

        // Validate the Move in the database
        List<Move> moveList = moveRepository.findAll();
        assertThat(moveList).hasSize(databaseSizeBeforeUpdate);
        Move testMove = moveList.get(moveList.size() - 1);
        assertThat(testMove.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testMove.getPower()).isEqualTo(UPDATED_POWER);
        assertThat(testMove.getAccuracy()).isEqualTo(UPDATED_ACCURACY);
        assertThat(testMove.getPp()).isEqualTo(UPDATED_PP);
        assertThat(testMove.isContact()).isEqualTo(UPDATED_CONTACT);
    }

    @Test
    @Transactional
    public void updateNonExistingMove() throws Exception {
        int databaseSizeBeforeUpdate = moveRepository.findAll().size();

        // Create the Move

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restMoveMockMvc.perform(put("/api/moves")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(move)))
            .andExpect(status().isBadRequest());

        // Validate the Move in the database
        List<Move> moveList = moveRepository.findAll();
        assertThat(moveList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteMove() throws Exception {
        // Initialize the database
        moveRepository.saveAndFlush(move);

        int databaseSizeBeforeDelete = moveRepository.findAll().size();

        // Delete the move
        restMoveMockMvc.perform(delete("/api/moves/{id}", move.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<Move> moveList = moveRepository.findAll();
        assertThat(moveList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Move.class);
        Move move1 = new Move();
        move1.setId(1L);
        Move move2 = new Move();
        move2.setId(move1.getId());
        assertThat(move1).isEqualTo(move2);
        move2.setId(2L);
        assertThat(move1).isNotEqualTo(move2);
        move1.setId(null);
        assertThat(move1).isNotEqualTo(move2);
    }
}
