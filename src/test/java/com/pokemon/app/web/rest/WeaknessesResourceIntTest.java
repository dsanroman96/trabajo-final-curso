package com.pokemon.app.web.rest;

import com.pokemon.app.PokemonBattleApp;

import com.pokemon.app.domain.Weaknesses;
import com.pokemon.app.repository.WeaknessesRepository;
import com.pokemon.app.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.util.ArrayList;
import java.util.List;


import static com.pokemon.app.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the WeaknessesResource REST controller.
 *
 * @see WeaknessesResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = PokemonBattleApp.class)
public class WeaknessesResourceIntTest {

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    @Autowired
    private WeaknessesRepository weaknessesRepository;

    @Mock
    private WeaknessesRepository weaknessesRepositoryMock;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restWeaknessesMockMvc;

    private Weaknesses weaknesses;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final WeaknessesResource weaknessesResource = new WeaknessesResource(weaknessesRepository);
        this.restWeaknessesMockMvc = MockMvcBuilders.standaloneSetup(weaknessesResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Weaknesses createEntity(EntityManager em) {
        Weaknesses weaknesses = new Weaknesses()
            .name(DEFAULT_NAME);
        return weaknesses;
    }

    @Before
    public void initTest() {
        weaknesses = createEntity(em);
    }

    @Test
    @Transactional
    public void createWeaknesses() throws Exception {
        int databaseSizeBeforeCreate = weaknessesRepository.findAll().size();

        // Create the Weaknesses
        restWeaknessesMockMvc.perform(post("/api/weaknesses")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(weaknesses)))
            .andExpect(status().isCreated());

        // Validate the Weaknesses in the database
        List<Weaknesses> weaknessesList = weaknessesRepository.findAll();
        assertThat(weaknessesList).hasSize(databaseSizeBeforeCreate + 1);
        Weaknesses testWeaknesses = weaknessesList.get(weaknessesList.size() - 1);
        assertThat(testWeaknesses.getName()).isEqualTo(DEFAULT_NAME);
    }

    @Test
    @Transactional
    public void createWeaknessesWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = weaknessesRepository.findAll().size();

        // Create the Weaknesses with an existing ID
        weaknesses.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restWeaknessesMockMvc.perform(post("/api/weaknesses")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(weaknesses)))
            .andExpect(status().isBadRequest());

        // Validate the Weaknesses in the database
        List<Weaknesses> weaknessesList = weaknessesRepository.findAll();
        assertThat(weaknessesList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllWeaknesses() throws Exception {
        // Initialize the database
        weaknessesRepository.saveAndFlush(weaknesses);

        // Get all the weaknessesList
        restWeaknessesMockMvc.perform(get("/api/weaknesses?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(weaknesses.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())));
    }
    
    @SuppressWarnings({"unchecked"})
    public void getAllWeaknessesWithEagerRelationshipsIsEnabled() throws Exception {
        WeaknessesResource weaknessesResource = new WeaknessesResource(weaknessesRepositoryMock);
        when(weaknessesRepositoryMock.findAllWithEagerRelationships(any())).thenReturn(new PageImpl(new ArrayList<>()));

        MockMvc restWeaknessesMockMvc = MockMvcBuilders.standaloneSetup(weaknessesResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();

        restWeaknessesMockMvc.perform(get("/api/weaknesses?eagerload=true"))
        .andExpect(status().isOk());

        verify(weaknessesRepositoryMock, times(1)).findAllWithEagerRelationships(any());
    }

    @SuppressWarnings({"unchecked"})
    public void getAllWeaknessesWithEagerRelationshipsIsNotEnabled() throws Exception {
        WeaknessesResource weaknessesResource = new WeaknessesResource(weaknessesRepositoryMock);
            when(weaknessesRepositoryMock.findAllWithEagerRelationships(any())).thenReturn(new PageImpl(new ArrayList<>()));
            MockMvc restWeaknessesMockMvc = MockMvcBuilders.standaloneSetup(weaknessesResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();

        restWeaknessesMockMvc.perform(get("/api/weaknesses?eagerload=true"))
        .andExpect(status().isOk());

            verify(weaknessesRepositoryMock, times(1)).findAllWithEagerRelationships(any());
    }

    @Test
    @Transactional
    public void getWeaknesses() throws Exception {
        // Initialize the database
        weaknessesRepository.saveAndFlush(weaknesses);

        // Get the weaknesses
        restWeaknessesMockMvc.perform(get("/api/weaknesses/{id}", weaknesses.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(weaknesses.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingWeaknesses() throws Exception {
        // Get the weaknesses
        restWeaknessesMockMvc.perform(get("/api/weaknesses/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateWeaknesses() throws Exception {
        // Initialize the database
        weaknessesRepository.saveAndFlush(weaknesses);

        int databaseSizeBeforeUpdate = weaknessesRepository.findAll().size();

        // Update the weaknesses
        Weaknesses updatedWeaknesses = weaknessesRepository.findById(weaknesses.getId()).get();
        // Disconnect from session so that the updates on updatedWeaknesses are not directly saved in db
        em.detach(updatedWeaknesses);
        updatedWeaknesses
            .name(UPDATED_NAME);

        restWeaknessesMockMvc.perform(put("/api/weaknesses")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedWeaknesses)))
            .andExpect(status().isOk());

        // Validate the Weaknesses in the database
        List<Weaknesses> weaknessesList = weaknessesRepository.findAll();
        assertThat(weaknessesList).hasSize(databaseSizeBeforeUpdate);
        Weaknesses testWeaknesses = weaknessesList.get(weaknessesList.size() - 1);
        assertThat(testWeaknesses.getName()).isEqualTo(UPDATED_NAME);
    }

    @Test
    @Transactional
    public void updateNonExistingWeaknesses() throws Exception {
        int databaseSizeBeforeUpdate = weaknessesRepository.findAll().size();

        // Create the Weaknesses

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restWeaknessesMockMvc.perform(put("/api/weaknesses")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(weaknesses)))
            .andExpect(status().isBadRequest());

        // Validate the Weaknesses in the database
        List<Weaknesses> weaknessesList = weaknessesRepository.findAll();
        assertThat(weaknessesList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteWeaknesses() throws Exception {
        // Initialize the database
        weaknessesRepository.saveAndFlush(weaknesses);

        int databaseSizeBeforeDelete = weaknessesRepository.findAll().size();

        // Delete the weaknesses
        restWeaknessesMockMvc.perform(delete("/api/weaknesses/{id}", weaknesses.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<Weaknesses> weaknessesList = weaknessesRepository.findAll();
        assertThat(weaknessesList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Weaknesses.class);
        Weaknesses weaknesses1 = new Weaknesses();
        weaknesses1.setId(1L);
        Weaknesses weaknesses2 = new Weaknesses();
        weaknesses2.setId(weaknesses1.getId());
        assertThat(weaknesses1).isEqualTo(weaknesses2);
        weaknesses2.setId(2L);
        assertThat(weaknesses1).isNotEqualTo(weaknesses2);
        weaknesses1.setId(null);
        assertThat(weaknesses1).isNotEqualTo(weaknesses2);
    }
}
