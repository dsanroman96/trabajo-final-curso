package com.pokemon.app.web.rest;

import com.pokemon.app.PokemonBattleApp;

import com.pokemon.app.domain.Resistances;
import com.pokemon.app.repository.ResistancesRepository;
import com.pokemon.app.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.util.ArrayList;
import java.util.List;


import static com.pokemon.app.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the ResistancesResource REST controller.
 *
 * @see ResistancesResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = PokemonBattleApp.class)
public class ResistancesResourceIntTest {

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    @Autowired
    private ResistancesRepository resistancesRepository;

    @Mock
    private ResistancesRepository resistancesRepositoryMock;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restResistancesMockMvc;

    private Resistances resistances;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final ResistancesResource resistancesResource = new ResistancesResource(resistancesRepository);
        this.restResistancesMockMvc = MockMvcBuilders.standaloneSetup(resistancesResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Resistances createEntity(EntityManager em) {
        Resistances resistances = new Resistances()
            .name(DEFAULT_NAME);
        return resistances;
    }

    @Before
    public void initTest() {
        resistances = createEntity(em);
    }

    @Test
    @Transactional
    public void createResistances() throws Exception {
        int databaseSizeBeforeCreate = resistancesRepository.findAll().size();

        // Create the Resistances
        restResistancesMockMvc.perform(post("/api/resistances")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(resistances)))
            .andExpect(status().isCreated());

        // Validate the Resistances in the database
        List<Resistances> resistancesList = resistancesRepository.findAll();
        assertThat(resistancesList).hasSize(databaseSizeBeforeCreate + 1);
        Resistances testResistances = resistancesList.get(resistancesList.size() - 1);
        assertThat(testResistances.getName()).isEqualTo(DEFAULT_NAME);
    }

    @Test
    @Transactional
    public void createResistancesWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = resistancesRepository.findAll().size();

        // Create the Resistances with an existing ID
        resistances.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restResistancesMockMvc.perform(post("/api/resistances")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(resistances)))
            .andExpect(status().isBadRequest());

        // Validate the Resistances in the database
        List<Resistances> resistancesList = resistancesRepository.findAll();
        assertThat(resistancesList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllResistances() throws Exception {
        // Initialize the database
        resistancesRepository.saveAndFlush(resistances);

        // Get all the resistancesList
        restResistancesMockMvc.perform(get("/api/resistances?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(resistances.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())));
    }
    
    @SuppressWarnings({"unchecked"})
    public void getAllResistancesWithEagerRelationshipsIsEnabled() throws Exception {
        ResistancesResource resistancesResource = new ResistancesResource(resistancesRepositoryMock);
        when(resistancesRepositoryMock.findAllWithEagerRelationships(any())).thenReturn(new PageImpl(new ArrayList<>()));

        MockMvc restResistancesMockMvc = MockMvcBuilders.standaloneSetup(resistancesResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();

        restResistancesMockMvc.perform(get("/api/resistances?eagerload=true"))
        .andExpect(status().isOk());

        verify(resistancesRepositoryMock, times(1)).findAllWithEagerRelationships(any());
    }

    @SuppressWarnings({"unchecked"})
    public void getAllResistancesWithEagerRelationshipsIsNotEnabled() throws Exception {
        ResistancesResource resistancesResource = new ResistancesResource(resistancesRepositoryMock);
            when(resistancesRepositoryMock.findAllWithEagerRelationships(any())).thenReturn(new PageImpl(new ArrayList<>()));
            MockMvc restResistancesMockMvc = MockMvcBuilders.standaloneSetup(resistancesResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();

        restResistancesMockMvc.perform(get("/api/resistances?eagerload=true"))
        .andExpect(status().isOk());

            verify(resistancesRepositoryMock, times(1)).findAllWithEagerRelationships(any());
    }

    @Test
    @Transactional
    public void getResistances() throws Exception {
        // Initialize the database
        resistancesRepository.saveAndFlush(resistances);

        // Get the resistances
        restResistancesMockMvc.perform(get("/api/resistances/{id}", resistances.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(resistances.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingResistances() throws Exception {
        // Get the resistances
        restResistancesMockMvc.perform(get("/api/resistances/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateResistances() throws Exception {
        // Initialize the database
        resistancesRepository.saveAndFlush(resistances);

        int databaseSizeBeforeUpdate = resistancesRepository.findAll().size();

        // Update the resistances
        Resistances updatedResistances = resistancesRepository.findById(resistances.getId()).get();
        // Disconnect from session so that the updates on updatedResistances are not directly saved in db
        em.detach(updatedResistances);
        updatedResistances
            .name(UPDATED_NAME);

        restResistancesMockMvc.perform(put("/api/resistances")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedResistances)))
            .andExpect(status().isOk());

        // Validate the Resistances in the database
        List<Resistances> resistancesList = resistancesRepository.findAll();
        assertThat(resistancesList).hasSize(databaseSizeBeforeUpdate);
        Resistances testResistances = resistancesList.get(resistancesList.size() - 1);
        assertThat(testResistances.getName()).isEqualTo(UPDATED_NAME);
    }

    @Test
    @Transactional
    public void updateNonExistingResistances() throws Exception {
        int databaseSizeBeforeUpdate = resistancesRepository.findAll().size();

        // Create the Resistances

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restResistancesMockMvc.perform(put("/api/resistances")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(resistances)))
            .andExpect(status().isBadRequest());

        // Validate the Resistances in the database
        List<Resistances> resistancesList = resistancesRepository.findAll();
        assertThat(resistancesList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteResistances() throws Exception {
        // Initialize the database
        resistancesRepository.saveAndFlush(resistances);

        int databaseSizeBeforeDelete = resistancesRepository.findAll().size();

        // Delete the resistances
        restResistancesMockMvc.perform(delete("/api/resistances/{id}", resistances.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<Resistances> resistancesList = resistancesRepository.findAll();
        assertThat(resistancesList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Resistances.class);
        Resistances resistances1 = new Resistances();
        resistances1.setId(1L);
        Resistances resistances2 = new Resistances();
        resistances2.setId(resistances1.getId());
        assertThat(resistances1).isEqualTo(resistances2);
        resistances2.setId(2L);
        assertThat(resistances1).isNotEqualTo(resistances2);
        resistances1.setId(null);
        assertThat(resistances1).isNotEqualTo(resistances2);
    }
}
