import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { UserRouteAccessService } from 'app/core';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { Resistances } from 'app/shared/model/resistances.model';
import { ResistancesService } from './resistances.service';
import { ResistancesComponent } from './resistances.component';
import { ResistancesDetailComponent } from './resistances-detail.component';
import { ResistancesUpdateComponent } from './resistances-update.component';
import { ResistancesDeletePopupComponent } from './resistances-delete-dialog.component';
import { IResistances } from 'app/shared/model/resistances.model';

@Injectable({ providedIn: 'root' })
export class ResistancesResolve implements Resolve<IResistances> {
    constructor(private service: ResistancesService) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<IResistances> {
        const id = route.params['id'] ? route.params['id'] : null;
        if (id) {
            return this.service.find(id).pipe(
                filter((response: HttpResponse<Resistances>) => response.ok),
                map((resistances: HttpResponse<Resistances>) => resistances.body)
            );
        }
        return of(new Resistances());
    }
}

export const resistancesRoute: Routes = [
    {
        path: '',
        component: ResistancesComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'pokemonBattleApp.resistances.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: ':id/view',
        component: ResistancesDetailComponent,
        resolve: {
            resistances: ResistancesResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'pokemonBattleApp.resistances.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'new',
        component: ResistancesUpdateComponent,
        resolve: {
            resistances: ResistancesResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'pokemonBattleApp.resistances.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: ':id/edit',
        component: ResistancesUpdateComponent,
        resolve: {
            resistances: ResistancesResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'pokemonBattleApp.resistances.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const resistancesPopupRoute: Routes = [
    {
        path: ':id/delete',
        component: ResistancesDeletePopupComponent,
        resolve: {
            resistances: ResistancesResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'pokemonBattleApp.resistances.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
