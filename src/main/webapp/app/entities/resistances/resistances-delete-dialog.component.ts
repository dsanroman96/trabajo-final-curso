import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IResistances } from 'app/shared/model/resistances.model';
import { ResistancesService } from './resistances.service';

@Component({
    selector: 'jhi-resistances-delete-dialog',
    templateUrl: './resistances-delete-dialog.component.html'
})
export class ResistancesDeleteDialogComponent {
    resistances: IResistances;

    constructor(
        protected resistancesService: ResistancesService,
        public activeModal: NgbActiveModal,
        protected eventManager: JhiEventManager
    ) {}

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.resistancesService.delete(id).subscribe(response => {
            this.eventManager.broadcast({
                name: 'resistancesListModification',
                content: 'Deleted an resistances'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-resistances-delete-popup',
    template: ''
})
export class ResistancesDeletePopupComponent implements OnInit, OnDestroy {
    protected ngbModalRef: NgbModalRef;

    constructor(protected activatedRoute: ActivatedRoute, protected router: Router, protected modalService: NgbModal) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ resistances }) => {
            setTimeout(() => {
                this.ngbModalRef = this.modalService.open(ResistancesDeleteDialogComponent as Component, {
                    size: 'lg',
                    backdrop: 'static'
                });
                this.ngbModalRef.componentInstance.resistances = resistances;
                this.ngbModalRef.result.then(
                    result => {
                        this.router.navigate(['/resistances', { outlets: { popup: null } }]);
                        this.ngbModalRef = null;
                    },
                    reason => {
                        this.router.navigate(['/resistances', { outlets: { popup: null } }]);
                        this.ngbModalRef = null;
                    }
                );
            }, 0);
        });
    }

    ngOnDestroy() {
        this.ngbModalRef = null;
    }
}
