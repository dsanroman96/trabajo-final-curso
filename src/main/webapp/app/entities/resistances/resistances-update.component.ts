import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { JhiAlertService } from 'ng-jhipster';
import { IResistances } from 'app/shared/model/resistances.model';
import { ResistancesService } from './resistances.service';
import { IType } from 'app/shared/model/type.model';
import { TypeService } from 'app/entities/type';

@Component({
    selector: 'jhi-resistances-update',
    templateUrl: './resistances-update.component.html'
})
export class ResistancesUpdateComponent implements OnInit {
    resistances: IResistances;
    isSaving: boolean;

    types: IType[];

    constructor(
        protected jhiAlertService: JhiAlertService,
        protected resistancesService: ResistancesService,
        protected typeService: TypeService,
        protected activatedRoute: ActivatedRoute
    ) {}

    ngOnInit() {
        this.isSaving = false;
        this.activatedRoute.data.subscribe(({ resistances }) => {
            this.resistances = resistances;
        });
        this.typeService
            .query()
            .pipe(
                filter((mayBeOk: HttpResponse<IType[]>) => mayBeOk.ok),
                map((response: HttpResponse<IType[]>) => response.body)
            )
            .subscribe((res: IType[]) => (this.types = res), (res: HttpErrorResponse) => this.onError(res.message));
    }

    previousState() {
        window.history.back();
    }

    save() {
        this.isSaving = true;
        if (this.resistances.id !== undefined) {
            this.subscribeToSaveResponse(this.resistancesService.update(this.resistances));
        } else {
            this.subscribeToSaveResponse(this.resistancesService.create(this.resistances));
        }
    }

    protected subscribeToSaveResponse(result: Observable<HttpResponse<IResistances>>) {
        result.subscribe((res: HttpResponse<IResistances>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
    }

    protected onSaveSuccess() {
        this.isSaving = false;
        this.previousState();
    }

    protected onSaveError() {
        this.isSaving = false;
    }

    protected onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }

    trackTypeById(index: number, item: IType) {
        return item.id;
    }

    getSelected(selectedVals: Array<any>, option: any) {
        if (selectedVals) {
            for (let i = 0; i < selectedVals.length; i++) {
                if (option.id === selectedVals[i].id) {
                    return selectedVals[i];
                }
            }
        }
        return option;
    }
}
