import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { IResistances } from 'app/shared/model/resistances.model';

type EntityResponseType = HttpResponse<IResistances>;
type EntityArrayResponseType = HttpResponse<IResistances[]>;

@Injectable({ providedIn: 'root' })
export class ResistancesService {
    public resourceUrl = SERVER_API_URL + 'api/resistances';

    constructor(protected http: HttpClient) {}

    create(resistances: IResistances): Observable<EntityResponseType> {
        return this.http.post<IResistances>(this.resourceUrl, resistances, { observe: 'response' });
    }

    update(resistances: IResistances): Observable<EntityResponseType> {
        return this.http.put<IResistances>(this.resourceUrl, resistances, { observe: 'response' });
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http.get<IResistances>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }

    query(req?: any): Observable<EntityArrayResponseType> {
        const options = createRequestOption(req);
        return this.http.get<IResistances[]>(this.resourceUrl, { params: options, observe: 'response' });
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }
}
