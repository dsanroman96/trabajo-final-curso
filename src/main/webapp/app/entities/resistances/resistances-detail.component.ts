import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IResistances } from 'app/shared/model/resistances.model';

@Component({
    selector: 'jhi-resistances-detail',
    templateUrl: './resistances-detail.component.html'
})
export class ResistancesDetailComponent implements OnInit {
    resistances: IResistances;

    constructor(protected activatedRoute: ActivatedRoute) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ resistances }) => {
            this.resistances = resistances;
        });
    }

    previousState() {
        window.history.back();
    }
}
