import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';
import { JhiLanguageService } from 'ng-jhipster';
import { JhiLanguageHelper } from 'app/core';

import { PokemonBattleSharedModule } from 'app/shared';
import {
    ResistancesComponent,
    ResistancesDetailComponent,
    ResistancesUpdateComponent,
    ResistancesDeletePopupComponent,
    ResistancesDeleteDialogComponent,
    resistancesRoute,
    resistancesPopupRoute
} from './';

const ENTITY_STATES = [...resistancesRoute, ...resistancesPopupRoute];

@NgModule({
    imports: [PokemonBattleSharedModule, RouterModule.forChild(ENTITY_STATES)],
    declarations: [
        ResistancesComponent,
        ResistancesDetailComponent,
        ResistancesUpdateComponent,
        ResistancesDeleteDialogComponent,
        ResistancesDeletePopupComponent
    ],
    entryComponents: [ResistancesComponent, ResistancesUpdateComponent, ResistancesDeleteDialogComponent, ResistancesDeletePopupComponent],
    providers: [{ provide: JhiLanguageService, useClass: JhiLanguageService }],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class PokemonBattleResistancesModule {
    constructor(private languageService: JhiLanguageService, private languageHelper: JhiLanguageHelper) {
        this.languageHelper.language.subscribe((languageKey: string) => {
            if (languageKey !== undefined) {
                this.languageService.changeLanguage(languageKey);
            }
        });
    }
}
