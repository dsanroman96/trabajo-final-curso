import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { IResistances } from 'app/shared/model/resistances.model';
import { AccountService } from 'app/core';
import { ResistancesService } from './resistances.service';

@Component({
    selector: 'jhi-resistances',
    templateUrl: './resistances.component.html'
})
export class ResistancesComponent implements OnInit, OnDestroy {
    resistances: IResistances[];
    currentAccount: any;
    eventSubscriber: Subscription;

    constructor(
        protected resistancesService: ResistancesService,
        protected jhiAlertService: JhiAlertService,
        protected eventManager: JhiEventManager,
        protected accountService: AccountService
    ) {}

    loadAll() {
        this.resistancesService
            .query()
            .pipe(
                filter((res: HttpResponse<IResistances[]>) => res.ok),
                map((res: HttpResponse<IResistances[]>) => res.body)
            )
            .subscribe(
                (res: IResistances[]) => {
                    this.resistances = res;
                },
                (res: HttpErrorResponse) => this.onError(res.message)
            );
    }

    ngOnInit() {
        this.loadAll();
        this.accountService.identity().then(account => {
            this.currentAccount = account;
        });
        this.registerChangeInResistances();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: IResistances) {
        return item.id;
    }

    registerChangeInResistances() {
        this.eventSubscriber = this.eventManager.subscribe('resistancesListModification', response => this.loadAll());
    }

    protected onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }
}
