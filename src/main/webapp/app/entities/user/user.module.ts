import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';
import { JhiLanguageService } from 'ng-jhipster';
import { JhiLanguageHelper } from 'app/core';

import { PokemonBattleSharedModule } from 'app/shared';
import {
    UserComponent,
    UserDetailComponent,
    UserUpdateComponent,
    UserDeletePopupComponent,
    UserDeleteDialogComponent,
    userRoute,
    userPopupRoute
} from './';

const ENTITY_STATES = [...userRoute, ...userPopupRoute];

@NgModule({
    imports: [PokemonBattleSharedModule, RouterModule.forChild(ENTITY_STATES)],
    declarations: [UserComponent, UserDetailComponent, UserUpdateComponent, UserDeleteDialogComponent, UserDeletePopupComponent],
    entryComponents: [UserComponent, UserUpdateComponent, UserDeleteDialogComponent, UserDeletePopupComponent],
    providers: [{ provide: JhiLanguageService, useClass: JhiLanguageService }],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class PokemonBattleUserModule {
    constructor(private languageService: JhiLanguageService, private languageHelper: JhiLanguageHelper) {
        this.languageHelper.language.subscribe((languageKey: string) => {
            if (languageKey !== undefined) {
                this.languageService.changeLanguage(languageKey);
            }
        });
    }
}
