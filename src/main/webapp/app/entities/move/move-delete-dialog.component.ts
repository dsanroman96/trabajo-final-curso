import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IMove } from 'app/shared/model/move.model';
import { MoveService } from './move.service';

@Component({
    selector: 'jhi-move-delete-dialog',
    templateUrl: './move-delete-dialog.component.html'
})
export class MoveDeleteDialogComponent {
    move: IMove;

    constructor(protected moveService: MoveService, public activeModal: NgbActiveModal, protected eventManager: JhiEventManager) {}

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.moveService.delete(id).subscribe(response => {
            this.eventManager.broadcast({
                name: 'moveListModification',
                content: 'Deleted an move'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-move-delete-popup',
    template: ''
})
export class MoveDeletePopupComponent implements OnInit, OnDestroy {
    protected ngbModalRef: NgbModalRef;

    constructor(protected activatedRoute: ActivatedRoute, protected router: Router, protected modalService: NgbModal) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ move }) => {
            setTimeout(() => {
                this.ngbModalRef = this.modalService.open(MoveDeleteDialogComponent as Component, { size: 'lg', backdrop: 'static' });
                this.ngbModalRef.componentInstance.move = move;
                this.ngbModalRef.result.then(
                    result => {
                        this.router.navigate(['/move', { outlets: { popup: null } }]);
                        this.ngbModalRef = null;
                    },
                    reason => {
                        this.router.navigate(['/move', { outlets: { popup: null } }]);
                        this.ngbModalRef = null;
                    }
                );
            }, 0);
        });
    }

    ngOnDestroy() {
        this.ngbModalRef = null;
    }
}
