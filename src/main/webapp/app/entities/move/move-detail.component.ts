import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IMove } from 'app/shared/model/move.model';

@Component({
    selector: 'jhi-move-detail',
    templateUrl: './move-detail.component.html'
})
export class MoveDetailComponent implements OnInit {
    move: IMove;

    constructor(protected activatedRoute: ActivatedRoute) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ move }) => {
            this.move = move;
        });
    }

    previousState() {
        window.history.back();
    }
}
