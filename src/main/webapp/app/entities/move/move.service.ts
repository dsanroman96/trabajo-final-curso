import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { IMove } from 'app/shared/model/move.model';

type EntityResponseType = HttpResponse<IMove>;
type EntityArrayResponseType = HttpResponse<IMove[]>;

@Injectable({ providedIn: 'root' })
export class MoveService {
    public resourceUrl = SERVER_API_URL + 'api/moves';

    constructor(protected http: HttpClient) {}

    create(move: IMove): Observable<EntityResponseType> {
        return this.http.post<IMove>(this.resourceUrl, move, { observe: 'response' });
    }

    update(move: IMove): Observable<EntityResponseType> {
        return this.http.put<IMove>(this.resourceUrl, move, { observe: 'response' });
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http.get<IMove>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }

    query(req?: any): Observable<EntityArrayResponseType> {
        const options = createRequestOption(req);
        return this.http.get<IMove[]>(this.resourceUrl, { params: options, observe: 'response' });
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }
}
