export * from './move.service';
export * from './move-update.component';
export * from './move-delete-dialog.component';
export * from './move-detail.component';
export * from './move.component';
export * from './move.route';
