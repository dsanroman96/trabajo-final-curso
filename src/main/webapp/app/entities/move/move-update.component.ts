import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { JhiAlertService } from 'ng-jhipster';
import { IMove } from 'app/shared/model/move.model';
import { MoveService } from './move.service';
import { IType } from 'app/shared/model/type.model';
import { TypeService } from 'app/entities/type';

@Component({
    selector: 'jhi-move-update',
    templateUrl: './move-update.component.html'
})
export class MoveUpdateComponent implements OnInit {
    move: IMove;
    isSaving: boolean;

    types: IType[];

    constructor(
        protected jhiAlertService: JhiAlertService,
        protected moveService: MoveService,
        protected typeService: TypeService,
        protected activatedRoute: ActivatedRoute
    ) {}

    ngOnInit() {
        this.isSaving = false;
        this.activatedRoute.data.subscribe(({ move }) => {
            this.move = move;
        });
        this.typeService
            .query()
            .pipe(
                filter((mayBeOk: HttpResponse<IType[]>) => mayBeOk.ok),
                map((response: HttpResponse<IType[]>) => response.body)
            )
            .subscribe((res: IType[]) => (this.types = res), (res: HttpErrorResponse) => this.onError(res.message));
    }

    previousState() {
        window.history.back();
    }

    save() {
        this.isSaving = true;
        if (this.move.id !== undefined) {
            this.subscribeToSaveResponse(this.moveService.update(this.move));
        } else {
            this.subscribeToSaveResponse(this.moveService.create(this.move));
        }
    }

    protected subscribeToSaveResponse(result: Observable<HttpResponse<IMove>>) {
        result.subscribe((res: HttpResponse<IMove>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
    }

    protected onSaveSuccess() {
        this.isSaving = false;
        this.previousState();
    }

    protected onSaveError() {
        this.isSaving = false;
    }

    protected onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }

    trackTypeById(index: number, item: IType) {
        return item.id;
    }
}
