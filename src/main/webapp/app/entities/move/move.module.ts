import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';
import { JhiLanguageService } from 'ng-jhipster';
import { JhiLanguageHelper } from 'app/core';

import { PokemonBattleSharedModule } from 'app/shared';
import {
    MoveComponent,
    MoveDetailComponent,
    MoveUpdateComponent,
    MoveDeletePopupComponent,
    MoveDeleteDialogComponent,
    moveRoute,
    movePopupRoute
} from './';

const ENTITY_STATES = [...moveRoute, ...movePopupRoute];

@NgModule({
    imports: [PokemonBattleSharedModule, RouterModule.forChild(ENTITY_STATES)],
    declarations: [MoveComponent, MoveDetailComponent, MoveUpdateComponent, MoveDeleteDialogComponent, MoveDeletePopupComponent],
    entryComponents: [MoveComponent, MoveUpdateComponent, MoveDeleteDialogComponent, MoveDeletePopupComponent],
    providers: [{ provide: JhiLanguageService, useClass: JhiLanguageService }],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class PokemonBattleMoveModule {
    constructor(private languageService: JhiLanguageService, private languageHelper: JhiLanguageHelper) {
        this.languageHelper.language.subscribe((languageKey: string) => {
            if (languageKey !== undefined) {
                this.languageService.changeLanguage(languageKey);
            }
        });
    }
}
