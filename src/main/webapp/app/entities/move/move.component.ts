import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { IMove } from 'app/shared/model/move.model';
import { AccountService } from 'app/core';
import { MoveService } from './move.service';

@Component({
    selector: 'jhi-move',
    templateUrl: './move.component.html'
})
export class MoveComponent implements OnInit, OnDestroy {
    moves: IMove[];
    currentAccount: any;
    eventSubscriber: Subscription;

    constructor(
        protected moveService: MoveService,
        protected jhiAlertService: JhiAlertService,
        protected eventManager: JhiEventManager,
        protected accountService: AccountService
    ) {}

    loadAll() {
        this.moveService
            .query()
            .pipe(
                filter((res: HttpResponse<IMove[]>) => res.ok),
                map((res: HttpResponse<IMove[]>) => res.body)
            )
            .subscribe(
                (res: IMove[]) => {
                    this.moves = res;
                },
                (res: HttpErrorResponse) => this.onError(res.message)
            );
    }

    ngOnInit() {
        this.loadAll();
        this.accountService.identity().then(account => {
            this.currentAccount = account;
        });
        this.registerChangeInMoves();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: IMove) {
        return item.id;
    }

    registerChangeInMoves() {
        this.eventSubscriber = this.eventManager.subscribe('moveListModification', response => this.loadAll());
    }

    protected onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }
}
