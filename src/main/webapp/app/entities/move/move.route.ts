import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { UserRouteAccessService } from 'app/core';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { Move } from 'app/shared/model/move.model';
import { MoveService } from './move.service';
import { MoveComponent } from './move.component';
import { MoveDetailComponent } from './move-detail.component';
import { MoveUpdateComponent } from './move-update.component';
import { MoveDeletePopupComponent } from './move-delete-dialog.component';
import { IMove } from 'app/shared/model/move.model';

@Injectable({ providedIn: 'root' })
export class MoveResolve implements Resolve<IMove> {
    constructor(private service: MoveService) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<IMove> {
        const id = route.params['id'] ? route.params['id'] : null;
        if (id) {
            return this.service.find(id).pipe(
                filter((response: HttpResponse<Move>) => response.ok),
                map((move: HttpResponse<Move>) => move.body)
            );
        }
        return of(new Move());
    }
}

export const moveRoute: Routes = [
    {
        path: '',
        component: MoveComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'pokemonBattleApp.move.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: ':id/view',
        component: MoveDetailComponent,
        resolve: {
            move: MoveResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'pokemonBattleApp.move.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'new',
        component: MoveUpdateComponent,
        resolve: {
            move: MoveResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'pokemonBattleApp.move.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: ':id/edit',
        component: MoveUpdateComponent,
        resolve: {
            move: MoveResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'pokemonBattleApp.move.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const movePopupRoute: Routes = [
    {
        path: ':id/delete',
        component: MoveDeletePopupComponent,
        resolve: {
            move: MoveResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'pokemonBattleApp.move.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
