import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IPokemon } from 'app/shared/model/pokemon.model';
import { PokemonService } from './pokemon.service';

@Component({
    selector: 'jhi-pokemon-delete-dialog',
    templateUrl: './pokemon-delete-dialog.component.html'
})
export class PokemonDeleteDialogComponent {
    pokemon: IPokemon;

    constructor(protected pokemonService: PokemonService, public activeModal: NgbActiveModal, protected eventManager: JhiEventManager) {}

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.pokemonService.delete(id).subscribe(response => {
            this.eventManager.broadcast({
                name: 'pokemonListModification',
                content: 'Deleted an pokemon'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-pokemon-delete-popup',
    template: ''
})
export class PokemonDeletePopupComponent implements OnInit, OnDestroy {
    protected ngbModalRef: NgbModalRef;

    constructor(protected activatedRoute: ActivatedRoute, protected router: Router, protected modalService: NgbModal) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ pokemon }) => {
            setTimeout(() => {
                this.ngbModalRef = this.modalService.open(PokemonDeleteDialogComponent as Component, { size: 'lg', backdrop: 'static' });
                this.ngbModalRef.componentInstance.pokemon = pokemon;
                this.ngbModalRef.result.then(
                    result => {
                        this.router.navigate(['/pokemon', { outlets: { popup: null } }]);
                        this.ngbModalRef = null;
                    },
                    reason => {
                        this.router.navigate(['/pokemon', { outlets: { popup: null } }]);
                        this.ngbModalRef = null;
                    }
                );
            }, 0);
        });
    }

    ngOnDestroy() {
        this.ngbModalRef = null;
    }
}
