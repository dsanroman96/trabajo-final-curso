import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { UserRouteAccessService } from 'app/core';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { Pokemon } from 'app/shared/model/pokemon.model';
import { PokemonService } from './pokemon.service';
import { PokemonComponent } from './pokemon.component';
import { PokemonDetailComponent } from './pokemon-detail.component';
import { PokemonUpdateComponent } from './pokemon-update.component';
import { PokemonDeletePopupComponent } from './pokemon-delete-dialog.component';
import { IPokemon } from 'app/shared/model/pokemon.model';

@Injectable({ providedIn: 'root' })
export class PokemonResolve implements Resolve<IPokemon> {
    constructor(private service: PokemonService) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<IPokemon> {
        const id = route.params['id'] ? route.params['id'] : null;
        if (id) {
            return this.service.find(id).pipe(
                filter((response: HttpResponse<Pokemon>) => response.ok),
                map((pokemon: HttpResponse<Pokemon>) => pokemon.body)
            );
        }
        return of(new Pokemon());
    }
}

export const pokemonRoute: Routes = [
    {
        path: '',
        component: PokemonComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'pokemonBattleApp.pokemon.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: ':id/view',
        component: PokemonDetailComponent,
        resolve: {
            pokemon: PokemonResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'pokemonBattleApp.pokemon.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'new',
        component: PokemonUpdateComponent,
        resolve: {
            pokemon: PokemonResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'pokemonBattleApp.pokemon.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: ':id/edit',
        component: PokemonUpdateComponent,
        resolve: {
            pokemon: PokemonResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'pokemonBattleApp.pokemon.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const pokemonPopupRoute: Routes = [
    {
        path: ':id/delete',
        component: PokemonDeletePopupComponent,
        resolve: {
            pokemon: PokemonResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'pokemonBattleApp.pokemon.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
