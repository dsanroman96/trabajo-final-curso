import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { IPokemon } from 'app/shared/model/pokemon.model';
import { AccountService } from 'app/core';
import { PokemonService } from './pokemon.service';

@Component({
    selector: 'jhi-pokemon',
    templateUrl: './pokemon.component.html'
})
export class PokemonComponent implements OnInit, OnDestroy {
    pokemons: IPokemon[];
    currentAccount: any;
    eventSubscriber: Subscription;

    constructor(
        protected pokemonService: PokemonService,
        protected jhiAlertService: JhiAlertService,
        protected eventManager: JhiEventManager,
        protected accountService: AccountService
    ) {}

    loadAll() {
        this.pokemonService
            .query()
            .pipe(
                filter((res: HttpResponse<IPokemon[]>) => res.ok),
                map((res: HttpResponse<IPokemon[]>) => res.body)
            )
            .subscribe(
                (res: IPokemon[]) => {
                    this.pokemons = res;
                },
                (res: HttpErrorResponse) => this.onError(res.message)
            );
    }

    ngOnInit() {
        this.loadAll();
        this.accountService.identity().then(account => {
            this.currentAccount = account;
        });
        this.registerChangeInPokemons();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: IPokemon) {
        return item.id;
    }

    registerChangeInPokemons() {
        this.eventSubscriber = this.eventManager.subscribe('pokemonListModification', response => this.loadAll());
    }

    protected onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }
}
