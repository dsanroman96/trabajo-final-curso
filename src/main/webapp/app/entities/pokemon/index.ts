export * from './pokemon.service';
export * from './pokemon-update.component';
export * from './pokemon-delete-dialog.component';
export * from './pokemon-detail.component';
export * from './pokemon.component';
export * from './pokemon.route';
