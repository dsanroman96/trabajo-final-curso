import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { JhiAlertService } from 'ng-jhipster';
import { IPokemon } from 'app/shared/model/pokemon.model';
import { PokemonService } from './pokemon.service';
import { IType } from 'app/shared/model/type.model';
import { TypeService } from 'app/entities/type';
import { IMove } from 'app/shared/model/move.model';
import { MoveService } from 'app/entities/move';

@Component({
    selector: 'jhi-pokemon-update',
    templateUrl: './pokemon-update.component.html'
})
export class PokemonUpdateComponent implements OnInit {
    pokemon: IPokemon;
    isSaving: boolean;

    types: IType[];

    moves: IMove[];

    constructor(
        protected jhiAlertService: JhiAlertService,
        protected pokemonService: PokemonService,
        protected typeService: TypeService,
        protected moveService: MoveService,
        protected activatedRoute: ActivatedRoute
    ) {}

    ngOnInit() {
        this.isSaving = false;
        this.activatedRoute.data.subscribe(({ pokemon }) => {
            this.pokemon = pokemon;
        });
        this.typeService
            .query()
            .pipe(
                filter((mayBeOk: HttpResponse<IType[]>) => mayBeOk.ok),
                map((response: HttpResponse<IType[]>) => response.body)
            )
            .subscribe((res: IType[]) => (this.types = res), (res: HttpErrorResponse) => this.onError(res.message));
        this.moveService
            .query()
            .pipe(
                filter((mayBeOk: HttpResponse<IMove[]>) => mayBeOk.ok),
                map((response: HttpResponse<IMove[]>) => response.body)
            )
            .subscribe((res: IMove[]) => (this.moves = res), (res: HttpErrorResponse) => this.onError(res.message));
    }

    previousState() {
        window.history.back();
    }

    save() {
        this.isSaving = true;
        if (this.pokemon.id !== undefined) {
            this.subscribeToSaveResponse(this.pokemonService.update(this.pokemon));
        } else {
            this.subscribeToSaveResponse(this.pokemonService.create(this.pokemon));
        }
    }

    protected subscribeToSaveResponse(result: Observable<HttpResponse<IPokemon>>) {
        result.subscribe((res: HttpResponse<IPokemon>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
    }

    protected onSaveSuccess() {
        this.isSaving = false;
        this.previousState();
    }

    protected onSaveError() {
        this.isSaving = false;
    }

    protected onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }

    trackTypeById(index: number, item: IType) {
        return item.id;
    }

    trackMoveById(index: number, item: IMove) {
        return item.id;
    }

    getSelected(selectedVals: Array<any>, option: any) {
        if (selectedVals) {
            for (let i = 0; i < selectedVals.length; i++) {
                if (option.id === selectedVals[i].id) {
                    return selectedVals[i];
                }
            }
        }
        return option;
    }
}
