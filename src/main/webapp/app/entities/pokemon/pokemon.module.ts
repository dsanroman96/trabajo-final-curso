import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';
import { JhiLanguageService } from 'ng-jhipster';
import { JhiLanguageHelper } from 'app/core';

import { PokemonBattleSharedModule } from 'app/shared';
import {
    PokemonComponent,
    PokemonDetailComponent,
    PokemonUpdateComponent,
    PokemonDeletePopupComponent,
    PokemonDeleteDialogComponent,
    pokemonRoute,
    pokemonPopupRoute
} from './';

const ENTITY_STATES = [...pokemonRoute, ...pokemonPopupRoute];

@NgModule({
    imports: [PokemonBattleSharedModule, RouterModule.forChild(ENTITY_STATES)],
    declarations: [
        PokemonComponent,
        PokemonDetailComponent,
        PokemonUpdateComponent,
        PokemonDeleteDialogComponent,
        PokemonDeletePopupComponent
    ],
    entryComponents: [PokemonComponent, PokemonUpdateComponent, PokemonDeleteDialogComponent, PokemonDeletePopupComponent],
    providers: [{ provide: JhiLanguageService, useClass: JhiLanguageService }],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class PokemonBattlePokemonModule {
    constructor(private languageService: JhiLanguageService, private languageHelper: JhiLanguageHelper) {
        this.languageHelper.language.subscribe((languageKey: string) => {
            if (languageKey !== undefined) {
                this.languageService.changeLanguage(languageKey);
            }
        });
    }
}
