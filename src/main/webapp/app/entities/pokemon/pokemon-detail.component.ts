import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IPokemon } from 'app/shared/model/pokemon.model';

@Component({
    selector: 'jhi-pokemon-detail',
    templateUrl: './pokemon-detail.component.html'
})
export class PokemonDetailComponent implements OnInit {
    pokemon: IPokemon;

    constructor(protected activatedRoute: ActivatedRoute) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ pokemon }) => {
            this.pokemon = pokemon;
        });
    }

    previousState() {
        window.history.back();
    }
}
