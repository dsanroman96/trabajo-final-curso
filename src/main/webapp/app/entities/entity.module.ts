import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

@NgModule({
    imports: [
        RouterModule.forChild([
            {
                path: 'type',
                loadChildren: './type/type.module#PokemonBattleTypeModule'
            },
            {
                path: 'move',
                loadChildren: './move/move.module#PokemonBattleMoveModule'
            },
            {
                path: 'pokemon',
                loadChildren: './pokemon/pokemon.module#PokemonBattlePokemonModule'
            },
            {
                path: 'user',
                loadChildren: './user/user.module#PokemonBattleUserModule'
            },
            {
                path: 'weaknesses',
                loadChildren: './weaknesses/weaknesses.module#PokemonBattleWeaknessesModule'
            },
            {
                path: 'type',
                loadChildren: './type/type.module#PokemonBattleTypeModule'
            },
            {
                path: 'resistances',
                loadChildren: './resistances/resistances.module#PokemonBattleResistancesModule'
            },
            {
                path: 'type',
                loadChildren: './type/type.module#PokemonBattleTypeModule'
            }
            /* jhipster-needle-add-entity-route - JHipster will add entity modules routes here */
        ])
    ],
    declarations: [],
    entryComponents: [],
    providers: [],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class PokemonBattleEntityModule {}
