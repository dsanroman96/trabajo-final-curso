import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { UserRouteAccessService } from 'app/core';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { Weaknesses } from 'app/shared/model/weaknesses.model';
import { WeaknessesService } from './weaknesses.service';
import { WeaknessesComponent } from './weaknesses.component';
import { WeaknessesDetailComponent } from './weaknesses-detail.component';
import { WeaknessesUpdateComponent } from './weaknesses-update.component';
import { WeaknessesDeletePopupComponent } from './weaknesses-delete-dialog.component';
import { IWeaknesses } from 'app/shared/model/weaknesses.model';

@Injectable({ providedIn: 'root' })
export class WeaknessesResolve implements Resolve<IWeaknesses> {
    constructor(private service: WeaknessesService) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<IWeaknesses> {
        const id = route.params['id'] ? route.params['id'] : null;
        if (id) {
            return this.service.find(id).pipe(
                filter((response: HttpResponse<Weaknesses>) => response.ok),
                map((weaknesses: HttpResponse<Weaknesses>) => weaknesses.body)
            );
        }
        return of(new Weaknesses());
    }
}

export const weaknessesRoute: Routes = [
    {
        path: '',
        component: WeaknessesComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'pokemonBattleApp.weaknesses.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: ':id/view',
        component: WeaknessesDetailComponent,
        resolve: {
            weaknesses: WeaknessesResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'pokemonBattleApp.weaknesses.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'new',
        component: WeaknessesUpdateComponent,
        resolve: {
            weaknesses: WeaknessesResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'pokemonBattleApp.weaknesses.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: ':id/edit',
        component: WeaknessesUpdateComponent,
        resolve: {
            weaknesses: WeaknessesResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'pokemonBattleApp.weaknesses.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const weaknessesPopupRoute: Routes = [
    {
        path: ':id/delete',
        component: WeaknessesDeletePopupComponent,
        resolve: {
            weaknesses: WeaknessesResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'pokemonBattleApp.weaknesses.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
