import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { JhiAlertService } from 'ng-jhipster';
import { IWeaknesses } from 'app/shared/model/weaknesses.model';
import { WeaknessesService } from './weaknesses.service';
import { IType } from 'app/shared/model/type.model';
import { TypeService } from 'app/entities/type';

@Component({
    selector: 'jhi-weaknesses-update',
    templateUrl: './weaknesses-update.component.html'
})
export class WeaknessesUpdateComponent implements OnInit {
    weaknesses: IWeaknesses;
    isSaving: boolean;

    types: IType[];

    constructor(
        protected jhiAlertService: JhiAlertService,
        protected weaknessesService: WeaknessesService,
        protected typeService: TypeService,
        protected activatedRoute: ActivatedRoute
    ) {}

    ngOnInit() {
        this.isSaving = false;
        this.activatedRoute.data.subscribe(({ weaknesses }) => {
            this.weaknesses = weaknesses;
        });
        this.typeService
            .query()
            .pipe(
                filter((mayBeOk: HttpResponse<IType[]>) => mayBeOk.ok),
                map((response: HttpResponse<IType[]>) => response.body)
            )
            .subscribe((res: IType[]) => (this.types = res), (res: HttpErrorResponse) => this.onError(res.message));
    }

    previousState() {
        window.history.back();
    }

    save() {
        this.isSaving = true;
        if (this.weaknesses.id !== undefined) {
            this.subscribeToSaveResponse(this.weaknessesService.update(this.weaknesses));
        } else {
            this.subscribeToSaveResponse(this.weaknessesService.create(this.weaknesses));
        }
    }

    protected subscribeToSaveResponse(result: Observable<HttpResponse<IWeaknesses>>) {
        result.subscribe((res: HttpResponse<IWeaknesses>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
    }

    protected onSaveSuccess() {
        this.isSaving = false;
        this.previousState();
    }

    protected onSaveError() {
        this.isSaving = false;
    }

    protected onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }

    trackTypeById(index: number, item: IType) {
        return item.id;
    }

    getSelected(selectedVals: Array<any>, option: any) {
        if (selectedVals) {
            for (let i = 0; i < selectedVals.length; i++) {
                if (option.id === selectedVals[i].id) {
                    return selectedVals[i];
                }
            }
        }
        return option;
    }
}
