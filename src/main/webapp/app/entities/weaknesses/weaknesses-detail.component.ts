import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IWeaknesses } from 'app/shared/model/weaknesses.model';

@Component({
    selector: 'jhi-weaknesses-detail',
    templateUrl: './weaknesses-detail.component.html'
})
export class WeaknessesDetailComponent implements OnInit {
    weaknesses: IWeaknesses;

    constructor(protected activatedRoute: ActivatedRoute) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ weaknesses }) => {
            this.weaknesses = weaknesses;
        });
    }

    previousState() {
        window.history.back();
    }
}
