export * from './weaknesses.service';
export * from './weaknesses-update.component';
export * from './weaknesses-delete-dialog.component';
export * from './weaknesses-detail.component';
export * from './weaknesses.component';
export * from './weaknesses.route';
