import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IWeaknesses } from 'app/shared/model/weaknesses.model';
import { WeaknessesService } from './weaknesses.service';

@Component({
    selector: 'jhi-weaknesses-delete-dialog',
    templateUrl: './weaknesses-delete-dialog.component.html'
})
export class WeaknessesDeleteDialogComponent {
    weaknesses: IWeaknesses;

    constructor(
        protected weaknessesService: WeaknessesService,
        public activeModal: NgbActiveModal,
        protected eventManager: JhiEventManager
    ) {}

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.weaknessesService.delete(id).subscribe(response => {
            this.eventManager.broadcast({
                name: 'weaknessesListModification',
                content: 'Deleted an weaknesses'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-weaknesses-delete-popup',
    template: ''
})
export class WeaknessesDeletePopupComponent implements OnInit, OnDestroy {
    protected ngbModalRef: NgbModalRef;

    constructor(protected activatedRoute: ActivatedRoute, protected router: Router, protected modalService: NgbModal) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ weaknesses }) => {
            setTimeout(() => {
                this.ngbModalRef = this.modalService.open(WeaknessesDeleteDialogComponent as Component, { size: 'lg', backdrop: 'static' });
                this.ngbModalRef.componentInstance.weaknesses = weaknesses;
                this.ngbModalRef.result.then(
                    result => {
                        this.router.navigate(['/weaknesses', { outlets: { popup: null } }]);
                        this.ngbModalRef = null;
                    },
                    reason => {
                        this.router.navigate(['/weaknesses', { outlets: { popup: null } }]);
                        this.ngbModalRef = null;
                    }
                );
            }, 0);
        });
    }

    ngOnDestroy() {
        this.ngbModalRef = null;
    }
}
