import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';
import { JhiLanguageService } from 'ng-jhipster';
import { JhiLanguageHelper } from 'app/core';

import { PokemonBattleSharedModule } from 'app/shared';
import {
    WeaknessesComponent,
    WeaknessesDetailComponent,
    WeaknessesUpdateComponent,
    WeaknessesDeletePopupComponent,
    WeaknessesDeleteDialogComponent,
    weaknessesRoute,
    weaknessesPopupRoute
} from './';

const ENTITY_STATES = [...weaknessesRoute, ...weaknessesPopupRoute];

@NgModule({
    imports: [PokemonBattleSharedModule, RouterModule.forChild(ENTITY_STATES)],
    declarations: [
        WeaknessesComponent,
        WeaknessesDetailComponent,
        WeaknessesUpdateComponent,
        WeaknessesDeleteDialogComponent,
        WeaknessesDeletePopupComponent
    ],
    entryComponents: [WeaknessesComponent, WeaknessesUpdateComponent, WeaknessesDeleteDialogComponent, WeaknessesDeletePopupComponent],
    providers: [{ provide: JhiLanguageService, useClass: JhiLanguageService }],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class PokemonBattleWeaknessesModule {
    constructor(private languageService: JhiLanguageService, private languageHelper: JhiLanguageHelper) {
        this.languageHelper.language.subscribe((languageKey: string) => {
            if (languageKey !== undefined) {
                this.languageService.changeLanguage(languageKey);
            }
        });
    }
}
