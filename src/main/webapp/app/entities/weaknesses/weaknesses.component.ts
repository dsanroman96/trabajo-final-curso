import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { IWeaknesses } from 'app/shared/model/weaknesses.model';
import { AccountService } from 'app/core';
import { WeaknessesService } from './weaknesses.service';

@Component({
    selector: 'jhi-weaknesses',
    templateUrl: './weaknesses.component.html'
})
export class WeaknessesComponent implements OnInit, OnDestroy {
    weaknesses: IWeaknesses[];
    currentAccount: any;
    eventSubscriber: Subscription;

    constructor(
        protected weaknessesService: WeaknessesService,
        protected jhiAlertService: JhiAlertService,
        protected eventManager: JhiEventManager,
        protected accountService: AccountService
    ) {}

    loadAll() {
        this.weaknessesService
            .query()
            .pipe(
                filter((res: HttpResponse<IWeaknesses[]>) => res.ok),
                map((res: HttpResponse<IWeaknesses[]>) => res.body)
            )
            .subscribe(
                (res: IWeaknesses[]) => {
                    this.weaknesses = res;
                },
                (res: HttpErrorResponse) => this.onError(res.message)
            );
    }

    ngOnInit() {
        this.loadAll();
        this.accountService.identity().then(account => {
            this.currentAccount = account;
        });
        this.registerChangeInWeaknesses();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: IWeaknesses) {
        return item.id;
    }

    registerChangeInWeaknesses() {
        this.eventSubscriber = this.eventManager.subscribe('weaknessesListModification', response => this.loadAll());
    }

    protected onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }
}
