import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { IWeaknesses } from 'app/shared/model/weaknesses.model';

type EntityResponseType = HttpResponse<IWeaknesses>;
type EntityArrayResponseType = HttpResponse<IWeaknesses[]>;

@Injectable({ providedIn: 'root' })
export class WeaknessesService {
    public resourceUrl = SERVER_API_URL + 'api/weaknesses';

    constructor(protected http: HttpClient) {}

    create(weaknesses: IWeaknesses): Observable<EntityResponseType> {
        return this.http.post<IWeaknesses>(this.resourceUrl, weaknesses, { observe: 'response' });
    }

    update(weaknesses: IWeaknesses): Observable<EntityResponseType> {
        return this.http.put<IWeaknesses>(this.resourceUrl, weaknesses, { observe: 'response' });
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http.get<IWeaknesses>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }

    query(req?: any): Observable<EntityArrayResponseType> {
        const options = createRequestOption(req);
        return this.http.get<IWeaknesses[]>(this.resourceUrl, { params: options, observe: 'response' });
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }
}
