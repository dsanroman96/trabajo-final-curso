import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { JhiAlertService } from 'ng-jhipster';
import { IType } from 'app/shared/model/type.model';
import { TypeService } from './type.service';
import { IWeaknesses } from 'app/shared/model/weaknesses.model';
import { WeaknessesService } from 'app/entities/weaknesses';
import { IResistances } from 'app/shared/model/resistances.model';
import { ResistancesService } from 'app/entities/resistances';

@Component({
    selector: 'jhi-type-update',
    templateUrl: './type-update.component.html'
})
export class TypeUpdateComponent implements OnInit {
    type: IType;
    isSaving: boolean;

    weaknesses: IWeaknesses[];

    resistances: IResistances[];

    constructor(
        protected jhiAlertService: JhiAlertService,
        protected typeService: TypeService,
        protected weaknessesService: WeaknessesService,
        protected resistancesService: ResistancesService,
        protected activatedRoute: ActivatedRoute
    ) {}

    ngOnInit() {
        this.isSaving = false;
        this.activatedRoute.data.subscribe(({ type }) => {
            this.type = type;
        });
        this.weaknessesService
            .query()
            .pipe(
                filter((mayBeOk: HttpResponse<IWeaknesses[]>) => mayBeOk.ok),
                map((response: HttpResponse<IWeaknesses[]>) => response.body)
            )
            .subscribe((res: IWeaknesses[]) => (this.weaknesses = res), (res: HttpErrorResponse) => this.onError(res.message));
        this.resistancesService
            .query()
            .pipe(
                filter((mayBeOk: HttpResponse<IResistances[]>) => mayBeOk.ok),
                map((response: HttpResponse<IResistances[]>) => response.body)
            )
            .subscribe((res: IResistances[]) => (this.resistances = res), (res: HttpErrorResponse) => this.onError(res.message));
    }

    previousState() {
        window.history.back();
    }

    save() {
        this.isSaving = true;
        if (this.type.id !== undefined) {
            this.subscribeToSaveResponse(this.typeService.update(this.type));
        } else {
            this.subscribeToSaveResponse(this.typeService.create(this.type));
        }
    }

    protected subscribeToSaveResponse(result: Observable<HttpResponse<IType>>) {
        result.subscribe((res: HttpResponse<IType>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
    }

    protected onSaveSuccess() {
        this.isSaving = false;
        this.previousState();
    }

    protected onSaveError() {
        this.isSaving = false;
    }

    protected onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }

    trackWeaknessesById(index: number, item: IWeaknesses) {
        return item.id;
    }

    trackResistancesById(index: number, item: IResistances) {
        return item.id;
    }
}
