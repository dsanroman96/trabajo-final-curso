export interface IUser {
    id?: number;
    victories?: number;
}

export class User implements IUser {
    constructor(public id?: number, public victories?: number) {}
}
