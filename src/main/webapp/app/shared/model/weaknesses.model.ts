import { IType } from 'app/shared/model/type.model';

export interface IWeaknesses {
    id?: number;
    name?: string;
    types?: IType[];
}

export class Weaknesses implements IWeaknesses {
    constructor(public id?: number, public name?: string, public types?: IType[]) {}
}
