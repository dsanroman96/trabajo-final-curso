import { IWeaknesses } from 'app/shared/model/weaknesses.model';
import { IResistances } from 'app/shared/model/resistances.model';

export interface IType {
    id?: number;
    name?: string;
    weaknesses?: IWeaknesses;
    resistances?: IResistances;
}

export class Type implements IType {
    constructor(public id?: number, public name?: string, public weaknesses?: IWeaknesses, public resistances?: IResistances) {}
}
