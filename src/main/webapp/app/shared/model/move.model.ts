import { IType } from 'app/shared/model/type.model';

export interface IMove {
    id?: number;
    name?: string;
    power?: number;
    accuracy?: number;
    pp?: number;
    contact?: boolean;
    type?: IType;
}

export class Move implements IMove {
    constructor(
        public id?: number,
        public name?: string,
        public power?: number,
        public accuracy?: number,
        public pp?: number,
        public contact?: boolean,
        public type?: IType
    ) {
        this.contact = this.contact || false;
    }
}
