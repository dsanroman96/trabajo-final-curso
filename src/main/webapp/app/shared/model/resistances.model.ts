import { IType } from 'app/shared/model/type.model';

export interface IResistances {
    id?: number;
    name?: string;
    types?: IType[];
}

export class Resistances implements IResistances {
    constructor(public id?: number, public name?: string, public types?: IType[]) {}
}
