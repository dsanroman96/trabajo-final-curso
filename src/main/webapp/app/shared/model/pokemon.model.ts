import { IType } from 'app/shared/model/type.model';
import { IMove } from 'app/shared/model/move.model';

export interface IPokemon {
    id?: number;
    name?: string;
    attack?: number;
    spAttack?: number;
    defense?: number;
    spDefense?: number;
    speed?: number;
    hp?: number;
    defeated?: boolean;
    types?: IType[];
    moves?: IMove[];
}

export class Pokemon implements IPokemon {
    constructor(
        public id?: number,
        public name?: string,
        public attack?: number,
        public spAttack?: number,
        public defense?: number,
        public spDefense?: number,
        public speed?: number,
        public hp?: number,
        public defeated?: boolean,
        public types?: IType[],
        public moves?: IMove[]
    ) {
        this.defeated = this.defeated || false;
    }
}
