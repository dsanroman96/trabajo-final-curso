package com.pokemon.app.domain;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;

import java.io.Serializable;
import java.util.Objects;

/**
 * A Move.
 */
@Entity
@Table(name = "move")
public class Move implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "power")
    private Integer power;

    @Column(name = "accuracy")
    private Integer accuracy;

    @Column(name = "pp")
    private Integer pp;

    @Column(name = "contact")
    private Boolean contact;

    @ManyToOne
    private Type type;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public Move name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getPower() {
        return power;
    }

    public Move power(Integer power) {
        this.power = power;
        return this;
    }

    public void setPower(Integer power) {
        this.power = power;
    }

    public Integer getAccuracy() {
        return accuracy;
    }

    public Move accuracy(Integer accuracy) {
        this.accuracy = accuracy;
        return this;
    }

    public void setAccuracy(Integer accuracy) {
        this.accuracy = accuracy;
    }

    public Integer getPp() {
        return pp;
    }

    public Move pp(Integer pp) {
        this.pp = pp;
        return this;
    }

    public void setPp(Integer pp) {
        this.pp = pp;
    }

    public Boolean isContact() {
        return contact;
    }

    public Move contact(Boolean contact) {
        this.contact = contact;
        return this;
    }

    public void setContact(Boolean contact) {
        this.contact = contact;
    }

    public Type getType() {
        return type;
    }

    public Move type(Type type) {
        this.type = type;
        return this;
    }

    public void setType(Type type) {
        this.type = type;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Move move = (Move) o;
        if (move.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), move.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Move{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", power=" + getPower() +
            ", accuracy=" + getAccuracy() +
            ", pp=" + getPp() +
            ", contact='" + isContact() + "'" +
            "}";
    }
}
