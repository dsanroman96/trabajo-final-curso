package com.pokemon.app.domain;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;

import java.io.Serializable;
import java.util.Objects;

/**
 * A Type.
 */
@Entity
@Table(name = "type")
public class Type implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "name")
    private String name;

    @ManyToOne
    @JsonIgnoreProperties("types")
    private Weaknesses weaknesses;

    @ManyToOne
    @JsonIgnoreProperties("types")
    private Resistances resistances;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public Type name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Weaknesses getWeaknesses() {
        return weaknesses;
    }

    public Type weaknesses(Weaknesses weaknesses) {
        this.weaknesses = weaknesses;
        return this;
    }

    public void setWeaknesses(Weaknesses weaknesses) {
        this.weaknesses = weaknesses;
    }

    public Resistances getResistances() {
        return resistances;
    }

    public Type resistances(Resistances resistances) {
        this.resistances = resistances;
        return this;
    }

    public void setResistances(Resistances resistances) {
        this.resistances = resistances;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Type type = (Type) o;
        if (type.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), type.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Type{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            "}";
    }
}
