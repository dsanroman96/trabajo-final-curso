package com.pokemon.app.domain;



import javax.persistence.*;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A Resistances.
 */
@Entity
@Table(name = "resistances")
public class Resistances implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "name")
    private String name;

    @ManyToMany
    @JoinTable(name = "resistances_type",
               joinColumns = @JoinColumn(name = "resistances_id", referencedColumnName = "id"),
               inverseJoinColumns = @JoinColumn(name = "type_id", referencedColumnName = "id"))
    private Set<Type> types = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public Resistances name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<Type> getTypes() {
        return types;
    }

    public Resistances types(Set<Type> types) {
        this.types = types;
        return this;
    }
/*
    public Resistances addType(Type type) {
        this.types.add(type);
        type.getResistances().add(this);
        return this;
    }

    public Resistances removeType(Type type) {
        this.types.remove(type);
        type.getResistances().remove(this);
        return this;
    }
*/
    public void setTypes(Set<Type> types) {
        this.types = types;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Resistances resistances = (Resistances) o;
        if (resistances.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), resistances.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Resistances{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            "}";
    }
}
