package com.pokemon.app.domain;



import javax.persistence.*;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A Weaknesses.
 */
@Entity
@Table(name = "weaknesses")
public class Weaknesses implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "name")
    private String name;

    @ManyToMany
    @JoinTable(name = "weaknesses_types",
               joinColumns = @JoinColumn(name = "weaknesses_id", referencedColumnName = "id"),
               inverseJoinColumns = @JoinColumn(name = "types_id", referencedColumnName = "id"))
    private Set<Type> types = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public Weaknesses name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<Type> getTypes() {
        return types;
    }

    public Weaknesses types(Set<Type> types) {
        this.types = types;
        return this;
    }
/*
    public Weaknesses addTypes(Type type) {
        this.types.add(type);
        type.getWeaknesses().add(this);
        return this;
    }

    public Weaknesses removeTypes(Type type) {
        this.types.remove(type);
        type.getWeaknesses().remove(this);
        return this;
    }
*/
    public void setTypes(Set<Type> types) {
        this.types = types;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Weaknesses weaknesses = (Weaknesses) o;
        if (weaknesses.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), weaknesses.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Weaknesses{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            "}";
    }
}
