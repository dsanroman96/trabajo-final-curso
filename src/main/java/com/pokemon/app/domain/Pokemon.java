package com.pokemon.app.domain;



import javax.persistence.*;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A Pokemon.
 */
@Entity
@Table(name = "pokemon")
public class Pokemon implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "attack")
    private Integer attack;

    @Column(name = "sp_attack")
    private Integer spAttack;

    @Column(name = "defense")
    private Integer defense;

    @Column(name = "sp_defense")
    private Integer spDefense;

    @Column(name = "speed")
    private Integer speed;

    @Column(name = "hp")
    private Integer hp;

    @Column(name = "defeated")
    private Boolean defeated;

    @ManyToMany
    @JoinTable(name = "pokemon_type",
               joinColumns = @JoinColumn(name = "pokemon_id", referencedColumnName = "id"),
               inverseJoinColumns = @JoinColumn(name = "type_id", referencedColumnName = "id"))
    private Set<Type> types = new HashSet<>();

    @ManyToMany
    @JoinTable(name = "pokemon_move",
               joinColumns = @JoinColumn(name = "pokemon_id", referencedColumnName = "id"),
               inverseJoinColumns = @JoinColumn(name = "move_id", referencedColumnName = "id"))
    private Set<Move> moves = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public Pokemon name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getAttack() {
        return attack;
    }

    public Pokemon attack(Integer attack) {
        this.attack = attack;
        return this;
    }

    public void setAttack(Integer attack) {
        this.attack = attack;
    }

    public Integer getSpAttack() {
        return spAttack;
    }

    public Pokemon spAttack(Integer spAttack) {
        this.spAttack = spAttack;
        return this;
    }

    public void setSpAttack(Integer spAttack) {
        this.spAttack = spAttack;
    }

    public Integer getDefense() {
        return defense;
    }

    public Pokemon defense(Integer defense) {
        this.defense = defense;
        return this;
    }

    public void setDefense(Integer defense) {
        this.defense = defense;
    }

    public Integer getSpDefense() {
        return spDefense;
    }

    public Pokemon spDefense(Integer spDefense) {
        this.spDefense = spDefense;
        return this;
    }

    public void setSpDefense(Integer spDefense) {
        this.spDefense = spDefense;
    }

    public Integer getSpeed() {
        return speed;
    }

    public Pokemon speed(Integer speed) {
        this.speed = speed;
        return this;
    }

    public void setSpeed(Integer speed) {
        this.speed = speed;
    }

    public Integer getHp() {
        return hp;
    }

    public Pokemon hp(Integer hp) {
        this.hp = hp;
        return this;
    }

    public void setHp(Integer hp) {
        this.hp = hp;
    }

    public Boolean isDefeated() {
        return defeated;
    }

    public Pokemon defeated(Boolean defeated) {
        this.defeated = defeated;
        return this;
    }

    public void setDefeated(Boolean defeated) {
        this.defeated = defeated;
    }

    public Set<Type> getTypes() {
        return types;
    }

    public Pokemon types(Set<Type> types) {
        this.types = types;
        return this;
    }
    
    /*
    public Pokemon addType(Type type) {
        this.types.add(type);
        type.getPokemons().add(this);
        return this;
    }

    public Pokemon removeType(Type type) {
        this.types.remove(type);
        type.getPokemons().remove(this);
        return this;
    }
    */

    public void setTypes(Set<Type> types) {
        this.types = types;
    }

    public Set<Move> getMoves() {
        return moves;
    }

    public Pokemon moves(Set<Move> moves) {
        this.moves = moves;
        return this;
    }

   /*
    public Pokemon addMove(Move move) {
        this.moves.add(move);
        move.getPokemons().add(this);
        return this;
    }

    public Pokemon removeMove(Move move) {
        this.moves.remove(move);
        move.getPokemons().remove(this);
        return this;
    }
    */

    public void setMoves(Set<Move> moves) {
        this.moves = moves;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Pokemon pokemon = (Pokemon) o;
        if (pokemon.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), pokemon.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Pokemon{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", attack=" + getAttack() +
            ", spAttack=" + getSpAttack() +
            ", defense=" + getDefense() +
            ", spDefense=" + getSpDefense() +
            ", speed=" + getSpeed() +
            ", hp=" + getHp() +
            ", defeated='" + isDefeated() + "'" +
            "}";
    }
}
