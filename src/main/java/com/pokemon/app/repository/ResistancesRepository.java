package com.pokemon.app.repository;

import com.pokemon.app.domain.Resistances;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * Spring Data  repository for the Resistances entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ResistancesRepository extends JpaRepository<Resistances, Long> {

    @Query(value = "select distinct resistances from Resistances resistances left join fetch resistances.types",
        countQuery = "select count(distinct resistances) from Resistances resistances")
    Page<Resistances> findAllWithEagerRelationships(Pageable pageable);

    @Query(value = "select distinct resistances from Resistances resistances left join fetch resistances.types")
    List<Resistances> findAllWithEagerRelationships();

    @Query("select resistances from Resistances resistances left join fetch resistances.types where resistances.id =:id")
    Optional<Resistances> findOneWithEagerRelationships(@Param("id") Long id);

}
