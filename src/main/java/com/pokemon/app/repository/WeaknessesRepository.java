package com.pokemon.app.repository;

import com.pokemon.app.domain.Weaknesses;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * Spring Data  repository for the Weaknesses entity.
 */
@SuppressWarnings("unused")
@Repository
public interface WeaknessesRepository extends JpaRepository<Weaknesses, Long> {

    @Query(value = "select distinct weaknesses from Weaknesses weaknesses left join fetch weaknesses.types",
        countQuery = "select count(distinct weaknesses) from Weaknesses weaknesses")
    Page<Weaknesses> findAllWithEagerRelationships(Pageable pageable);

    @Query(value = "select distinct weaknesses from Weaknesses weaknesses left join fetch weaknesses.types")
    List<Weaknesses> findAllWithEagerRelationships();

    @Query("select weaknesses from Weaknesses weaknesses left join fetch weaknesses.types where weaknesses.id =:id")
    Optional<Weaknesses> findOneWithEagerRelationships(@Param("id") Long id);

}
