package com.pokemon.app.repository;

import com.pokemon.app.domain.Pokemon;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * Spring Data  repository for the Pokemon entity.
 */
@SuppressWarnings("unused")
@Repository
public interface PokemonRepository extends JpaRepository<Pokemon, Long> {

    @Query(value = "select distinct pokemon from Pokemon pokemon left join fetch pokemon.types left join fetch pokemon.moves",
        countQuery = "select count(distinct pokemon) from Pokemon pokemon")
    Page<Pokemon> findAllWithEagerRelationships(Pageable pageable);

    @Query(value = "select distinct pokemon from Pokemon pokemon left join fetch pokemon.types left join fetch pokemon.moves")
    List<Pokemon> findAllWithEagerRelationships();

    @Query("select pokemon from Pokemon pokemon left join fetch pokemon.types left join fetch pokemon.moves where pokemon.id =:id")
    Optional<Pokemon> findOneWithEagerRelationships(@Param("id") Long id);

}
