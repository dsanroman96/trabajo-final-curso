package com.pokemon.app.web.rest;
import com.pokemon.app.domain.Weaknesses;
import com.pokemon.app.repository.WeaknessesRepository;
import com.pokemon.app.web.rest.errors.BadRequestAlertException;
import com.pokemon.app.web.rest.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing Weaknesses.
 */
@RestController
@RequestMapping("/api")
public class WeaknessesResource {

    private final Logger log = LoggerFactory.getLogger(WeaknessesResource.class);

    private static final String ENTITY_NAME = "weaknesses";

    private final WeaknessesRepository weaknessesRepository;

    public WeaknessesResource(WeaknessesRepository weaknessesRepository) {
        this.weaknessesRepository = weaknessesRepository;
    }

    /**
     * POST  /weaknesses : Create a new weaknesses.
     *
     * @param weaknesses the weaknesses to create
     * @return the ResponseEntity with status 201 (Created) and with body the new weaknesses, or with status 400 (Bad Request) if the weaknesses has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/weaknesses")
    public ResponseEntity<Weaknesses> createWeaknesses(@RequestBody Weaknesses weaknesses) throws URISyntaxException {
        log.debug("REST request to save Weaknesses : {}", weaknesses);
        if (weaknesses.getId() != null) {
            throw new BadRequestAlertException("A new weaknesses cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Weaknesses result = weaknessesRepository.save(weaknesses);
        return ResponseEntity.created(new URI("/api/weaknesses/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /weaknesses : Updates an existing weaknesses.
     *
     * @param weaknesses the weaknesses to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated weaknesses,
     * or with status 400 (Bad Request) if the weaknesses is not valid,
     * or with status 500 (Internal Server Error) if the weaknesses couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/weaknesses")
    public ResponseEntity<Weaknesses> updateWeaknesses(@RequestBody Weaknesses weaknesses) throws URISyntaxException {
        log.debug("REST request to update Weaknesses : {}", weaknesses);
        if (weaknesses.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        Weaknesses result = weaknessesRepository.save(weaknesses);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, weaknesses.getId().toString()))
            .body(result);
    }

    /**
     * GET  /weaknesses : get all the weaknesses.
     *
     * @param eagerload flag to eager load entities from relationships (This is applicable for many-to-many)
     * @return the ResponseEntity with status 200 (OK) and the list of weaknesses in body
     */
    @GetMapping("/weaknesses")
    public List<Weaknesses> getAllWeaknesses(@RequestParam(required = false, defaultValue = "false") boolean eagerload) {
        log.debug("REST request to get all Weaknesses");
        return weaknessesRepository.findAllWithEagerRelationships();
    }

    /**
     * GET  /weaknesses/:id : get the "id" weaknesses.
     *
     * @param id the id of the weaknesses to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the weaknesses, or with status 404 (Not Found)
     */
    @GetMapping("/weaknesses/{id}")
    public ResponseEntity<Weaknesses> getWeaknesses(@PathVariable Long id) {
        log.debug("REST request to get Weaknesses : {}", id);
        Optional<Weaknesses> weaknesses = weaknessesRepository.findOneWithEagerRelationships(id);
        return ResponseUtil.wrapOrNotFound(weaknesses);
    }

    /**
     * DELETE  /weaknesses/:id : delete the "id" weaknesses.
     *
     * @param id the id of the weaknesses to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/weaknesses/{id}")
    public ResponseEntity<Void> deleteWeaknesses(@PathVariable Long id) {
        log.debug("REST request to delete Weaknesses : {}", id);
        weaknessesRepository.deleteById(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
