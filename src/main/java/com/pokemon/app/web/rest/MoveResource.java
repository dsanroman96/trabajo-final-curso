package com.pokemon.app.web.rest;
import com.pokemon.app.domain.Move;
import com.pokemon.app.repository.MoveRepository;
import com.pokemon.app.web.rest.errors.BadRequestAlertException;
import com.pokemon.app.web.rest.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing Move.
 */
@RestController
@RequestMapping("/api")
public class MoveResource {

    private final Logger log = LoggerFactory.getLogger(MoveResource.class);

    private static final String ENTITY_NAME = "move";

    private final MoveRepository moveRepository;

    public MoveResource(MoveRepository moveRepository) {
        this.moveRepository = moveRepository;
    }

    /**
     * POST  /moves : Create a new move.
     *
     * @param move the move to create
     * @return the ResponseEntity with status 201 (Created) and with body the new move, or with status 400 (Bad Request) if the move has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/moves")
    public ResponseEntity<Move> createMove(@RequestBody Move move) throws URISyntaxException {
        log.debug("REST request to save Move : {}", move);
        if (move.getId() != null) {
            throw new BadRequestAlertException("A new move cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Move result = moveRepository.save(move);
        return ResponseEntity.created(new URI("/api/moves/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /moves : Updates an existing move.
     *
     * @param move the move to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated move,
     * or with status 400 (Bad Request) if the move is not valid,
     * or with status 500 (Internal Server Error) if the move couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/moves")
    public ResponseEntity<Move> updateMove(@RequestBody Move move) throws URISyntaxException {
        log.debug("REST request to update Move : {}", move);
        if (move.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        Move result = moveRepository.save(move);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, move.getId().toString()))
            .body(result);
    }

    /**
     * GET  /moves : get all the moves.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of moves in body
     */
    @GetMapping("/moves")
    public List<Move> getAllMoves() {
        log.debug("REST request to get all Moves");
        return moveRepository.findAll();
    }

    /**
     * GET  /moves/:id : get the "id" move.
     *
     * @param id the id of the move to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the move, or with status 404 (Not Found)
     */
    @GetMapping("/moves/{id}")
    public ResponseEntity<Move> getMove(@PathVariable Long id) {
        log.debug("REST request to get Move : {}", id);
        Optional<Move> move = moveRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(move);
    }

    /**
     * DELETE  /moves/:id : delete the "id" move.
     *
     * @param id the id of the move to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/moves/{id}")
    public ResponseEntity<Void> deleteMove(@PathVariable Long id) {
        log.debug("REST request to delete Move : {}", id);
        moveRepository.deleteById(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
