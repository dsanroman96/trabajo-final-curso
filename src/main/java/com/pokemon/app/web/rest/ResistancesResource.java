package com.pokemon.app.web.rest;
import com.pokemon.app.domain.Resistances;
import com.pokemon.app.repository.ResistancesRepository;
import com.pokemon.app.web.rest.errors.BadRequestAlertException;
import com.pokemon.app.web.rest.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing Resistances.
 */
@RestController
@RequestMapping("/api")
public class ResistancesResource {

    private final Logger log = LoggerFactory.getLogger(ResistancesResource.class);

    private static final String ENTITY_NAME = "resistances";

    private final ResistancesRepository resistancesRepository;

    public ResistancesResource(ResistancesRepository resistancesRepository) {
        this.resistancesRepository = resistancesRepository;
    }

    /**
     * POST  /resistances : Create a new resistances.
     *
     * @param resistances the resistances to create
     * @return the ResponseEntity with status 201 (Created) and with body the new resistances, or with status 400 (Bad Request) if the resistances has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/resistances")
    public ResponseEntity<Resistances> createResistances(@RequestBody Resistances resistances) throws URISyntaxException {
        log.debug("REST request to save Resistances : {}", resistances);
        if (resistances.getId() != null) {
            throw new BadRequestAlertException("A new resistances cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Resistances result = resistancesRepository.save(resistances);
        return ResponseEntity.created(new URI("/api/resistances/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /resistances : Updates an existing resistances.
     *
     * @param resistances the resistances to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated resistances,
     * or with status 400 (Bad Request) if the resistances is not valid,
     * or with status 500 (Internal Server Error) if the resistances couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/resistances")
    public ResponseEntity<Resistances> updateResistances(@RequestBody Resistances resistances) throws URISyntaxException {
        log.debug("REST request to update Resistances : {}", resistances);
        if (resistances.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        Resistances result = resistancesRepository.save(resistances);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, resistances.getId().toString()))
            .body(result);
    }

    /**
     * GET  /resistances : get all the resistances.
     *
     * @param eagerload flag to eager load entities from relationships (This is applicable for many-to-many)
     * @return the ResponseEntity with status 200 (OK) and the list of resistances in body
     */
    @GetMapping("/resistances")
    public List<Resistances> getAllResistances(@RequestParam(required = false, defaultValue = "false") boolean eagerload) {
        log.debug("REST request to get all Resistances");
        return resistancesRepository.findAllWithEagerRelationships();
    }

    /**
     * GET  /resistances/:id : get the "id" resistances.
     *
     * @param id the id of the resistances to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the resistances, or with status 404 (Not Found)
     */
    @GetMapping("/resistances/{id}")
    public ResponseEntity<Resistances> getResistances(@PathVariable Long id) {
        log.debug("REST request to get Resistances : {}", id);
        Optional<Resistances> resistances = resistancesRepository.findOneWithEagerRelationships(id);
        return ResponseUtil.wrapOrNotFound(resistances);
    }

    /**
     * DELETE  /resistances/:id : delete the "id" resistances.
     *
     * @param id the id of the resistances to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/resistances/{id}")
    public ResponseEntity<Void> deleteResistances(@PathVariable Long id) {
        log.debug("REST request to delete Resistances : {}", id);
        resistancesRepository.deleteById(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
