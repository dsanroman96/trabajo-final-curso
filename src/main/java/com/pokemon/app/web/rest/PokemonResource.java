package com.pokemon.app.web.rest;
import com.pokemon.app.domain.Pokemon;
import com.pokemon.app.repository.PokemonRepository;
import com.pokemon.app.web.rest.errors.BadRequestAlertException;
import com.pokemon.app.web.rest.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing Pokemon.
 */
@RestController
@RequestMapping("/api")
public class PokemonResource {

    private final Logger log = LoggerFactory.getLogger(PokemonResource.class);

    private static final String ENTITY_NAME = "pokemon";

    private final PokemonRepository pokemonRepository;

    public PokemonResource(PokemonRepository pokemonRepository) {
        this.pokemonRepository = pokemonRepository;
    }

    /**
     * POST  /pokemons : Create a new pokemon.
     *
     * @param pokemon the pokemon to create
     * @return the ResponseEntity with status 201 (Created) and with body the new pokemon, or with status 400 (Bad Request) if the pokemon has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/pokemons")
    public ResponseEntity<Pokemon> createPokemon(@RequestBody Pokemon pokemon) throws URISyntaxException {
        log.debug("REST request to save Pokemon : {}", pokemon);
        if (pokemon.getId() != null) {
            throw new BadRequestAlertException("A new pokemon cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Pokemon result = pokemonRepository.save(pokemon);
        return ResponseEntity.created(new URI("/api/pokemons/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /pokemons : Updates an existing pokemon.
     *
     * @param pokemon the pokemon to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated pokemon,
     * or with status 400 (Bad Request) if the pokemon is not valid,
     * or with status 500 (Internal Server Error) if the pokemon couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/pokemons")
    public ResponseEntity<Pokemon> updatePokemon(@RequestBody Pokemon pokemon) throws URISyntaxException {
        log.debug("REST request to update Pokemon : {}", pokemon);
        if (pokemon.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        Pokemon result = pokemonRepository.save(pokemon);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, pokemon.getId().toString()))
            .body(result);
    }

    /**
     * GET  /pokemons : get all the pokemons.
     *
     * @param eagerload flag to eager load entities from relationships (This is applicable for many-to-many)
     * @return the ResponseEntity with status 200 (OK) and the list of pokemons in body
     */
    @GetMapping("/pokemons")
    public List<Pokemon> getAllPokemons(@RequestParam(required = false, defaultValue = "false") boolean eagerload) {
        log.debug("REST request to get all Pokemons");
        return pokemonRepository.findAllWithEagerRelationships();
    }

    /**
     * GET  /pokemons/:id : get the "id" pokemon.
     *
     * @param id the id of the pokemon to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the pokemon, or with status 404 (Not Found)
     */
    @GetMapping("/pokemons/{id}")
    public ResponseEntity<Pokemon> getPokemon(@PathVariable Long id) {
        log.debug("REST request to get Pokemon : {}", id);
        Optional<Pokemon> pokemon = pokemonRepository.findOneWithEagerRelationships(id);
        return ResponseUtil.wrapOrNotFound(pokemon);
    }

    /**
     * DELETE  /pokemons/:id : delete the "id" pokemon.
     *
     * @param id the id of the pokemon to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/pokemons/{id}")
    public ResponseEntity<Void> deletePokemon(@PathVariable Long id) {
        log.debug("REST request to delete Pokemon : {}", id);
        pokemonRepository.deleteById(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
