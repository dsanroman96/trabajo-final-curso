/**
 * View Models used by Spring MVC REST controllers.
 */
package com.pokemon.app.web.rest.vm;
